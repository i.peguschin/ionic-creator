import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SafeResourceUrl } from '@angular/platform-browser/src/security/dom_sanitization_service';

@Pipe({
  name: 'trustedurl',
})
export class TrustedurlPipe implements PipeTransform {

  /**
   * Transforms a Value to SafeResourceUrl
   * @param _sanitizer
   */
  constructor(private _sanitizer: DomSanitizer) {
  }

  /**
   *
   * @param value
   * @param args
   * @returns SafeResourceUrl
   */
  transform(value: string, ...args): SafeResourceUrl {
    return this._sanitizer.bypassSecurityTrustResourceUrl(value);
  }
}
