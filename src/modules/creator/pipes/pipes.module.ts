import { NgModule } from '@angular/core';
import { KeysPipe } from './keys/keys';
import { SafehtmlPipe } from './safehtml/safehtml';
import { TrustedurlPipe } from './trustedurl/trustedurl';

@NgModule({
  declarations: [
    KeysPipe,
    SafehtmlPipe,
    TrustedurlPipe,
  ],
  imports     : [],
  exports     : [
    KeysPipe,
    SafehtmlPipe,
    TrustedurlPipe,
  ]
})
export class PipesModule {
}
