import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SafeHtml } from '@angular/platform-browser/src/security/dom_sanitization_service';


@Pipe({
  name: 'safehtml',
})
export class SafehtmlPipe implements PipeTransform {
  /**
   * Transforms a Value to SafeHtml
   * @param _sanitizer
   */
  constructor(private _sanitizer: DomSanitizer) {
  }

  /**
   * Bypass security and trust the given value to be SafeHtml value
   * @param value
   * @param args
   *
   */
  transform(value: string, ...args): SafeHtml {
    return this._sanitizer.bypassSecurityTrustHtml(value);
  }
}
