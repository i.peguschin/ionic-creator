import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the KeysPipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'keys',
})
export class KeysPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   * @param value
   * @param args
   *
   */
  transform(value, args: string[]) {
    return Object.keys(value).map(key => {
      return {key: key, value: value[key]};
    });
  }
}
