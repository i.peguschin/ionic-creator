import { Directive, ElementRef, EventEmitter, forwardRef, Input, Output } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { CreatorProvider } from '../../providers/creator/creator';

@Directive({
  selector : '[creator-text]',
  host     : {
    '(input)': 'onKeyup()'
  },
  providers: [
    {
      provide    : NG_VALUE_ACCESSOR,
      multi      : true,
      useExisting: forwardRef(() => CreatorTextDirective),
    }
  ]
})
export class CreatorTextDirective {

  private _onSwitchSubscription: Subscription;

  constructor(public creatorProv: CreatorProvider, private _el: ElementRef) {

  }

  _changeEditMode = (editMode) => {

    if (!this.disabled) {
      if (editMode) {
        this._el.nativeElement.setAttribute('contenteditable', true)
      } else {
        this._el.nativeElement.removeAttribute('contenteditable')
      }
    }


  };

  ngOnInit() {
    this._changeEditMode(this.creatorProv.editMode);
    this._onSwitchSubscription = this.creatorProv.OnSwitchEditMode.subscribe(this._changeEditMode)
  }


  ngOnDestroy() {
    this._onSwitchSubscription.unsubscribe();
  }

  /**
   * Standard Accessor methods
   */
  propagateChange = (_: any) => {
  };

  /**
   * Standard ngModel register function
   * @param fn
   */
  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  /**
   * Standard ngModel touched function
   */
  registerOnTouched() {
  }

  /**
   * Standard ngModel write function
   */
  writeValue(value: any) {
    if (value !== undefined) {
      this.inputValue                  = value;
      this._el.nativeElement.innerText = this.inputValue;
    }
  }

  // ----------------------

  /**
   * value input (ngModel)
   *

   */
  @Input() _inputValue; // notice the '_'

  /**
   * Change Output
   *
   */
  @Output('change') OnChange: EventEmitter<HTMLSelectElement> = new EventEmitter();

  /**
   * Disabled input
   *

   */
  @Input('disabled') private _disabled: boolean | string = false;

  /**
   * getter is disabled
   *
   */
  get disabled() {
    return (this._disabled === '') ? true : !!this._disabled;
  }

  /**
   * Getter input value
   *
   */
  get inputValue() {
    return this._inputValue;
  }

  /**
   * Setter input value
   * @param val
   */
  set inputValue(val) {
    this._inputValue = (isNaN(val)) ? val : Number.parseInt(val);
  }

  /**
   * manual change trigger
   */
  onKeyup() {

    this.inputValue = this._el.nativeElement.innerText;

    this.propagateChange(this._inputValue);
  }


}
