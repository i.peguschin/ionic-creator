import { InterfaceCreator } from '../interfaces/creator';


export class ModelCreatorWidget {

  type: string;

  constructor(widget) {
    this.type = widget.type;
  }

}


export class ModelCreatorWidgetHeadline extends ModelCreatorWidget {

  text: string;
  headline: number;

  constructor(widget) {

    super(widget);

    this.text     = widget.text || 'This is a headline!';
    this.headline = widget.headline || 1;
  }

}

export class ModelCreatorWidgetParagraph extends ModelCreatorWidget {

  text: string;

  constructor(widget) {
    super(widget);
    this.text = widget.text || '<p>Do you see any Teletubbies in here? Do you see a slender plastic tag clipped to my shirt with my name printed on it? Do you see a little Asian child with a blank expression on his face sitting outside on a mechanical helicopter that shakes when you put quarters in it? No? Well, that\'s what you see at a toy store. And you must think you\'re in a toy store, because you\'re here shopping for an infant named Jeb.</p>';
  }

}


export class ModelCreatorWidgetImage extends ModelCreatorWidget {

  src: string;
  link: string;
  linkType: string;

  constructor(widget) {
    super(widget);
    this.src      = widget.src || 'http://via.placeholder.com/350x150';
    this.link     = widget.link;
    this.linkType = widget.linkType;
  }

}

export class ModelCreatorWidgetDemo extends ModelCreatorWidget {

  foo: string;
  bar: string;

  constructor(widget) {
    super(widget);
    this.foo = widget.foo;
    this.bar = widget.bar || 1;
  }

}


export class ModelCreatorWidgetTable extends ModelCreatorWidget {

  cols: Array<{ widgets: Array<ModelCreatorWidget> }>;

  constructor(widget: InterfaceCreator.Widget) {

    super(widget);

    this.cols = widget.cols || [{widgets: []}, {widgets: []}];

    this.cols = this.cols.map(col => {

      col.widgets = col.widgets || []; //CREATORWIDGETMODELIZE(

      return col;

    });

  }

}

export class ModelCreatorWidgetPlace {

  id: string;
  data: {
    active: Date;
    name: string;
    widgets: Array<InterfaceCreator.Widget>
  };

  constructor(place: { id: string, data: any } = {
    id  : null,
    data: {}
  }) {
    this.id           = place.id;
    this.data         = place.data;
    this.data.active  = new Date(this.data.active);
    this.data.name    = this.data.name || '';
    this.data.widgets = this.data.widgets || [] //CREATORWIDGETMODELIZE(this.data.widgets || []);
  }

}







