import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/**
 * Local storage provider
 */
@Injectable()
export class LocalstorageProvider {
  /**
   * Local storage data object
   */
  public data: any;

  /**
   * Get local storage item
   * @param key
   * @param toJSON
   *
   */
  get(key, toJSON: boolean = true) {
    let c = localStorage.getItem(key);
    return (toJSON && c) ? JSON.parse(c) : localStorage.getItem(key)
  }

  /**
   * Set local storage item
   * @param key
   * @param data
   * @param toString
   */
  set(key, data, toString: boolean = true) {
    return (toString) ? localStorage.setItem(key, JSON.stringify(data)) : localStorage.setItem(key, data);
  }

  /**
   * Remove local storage item
   * @param key
   */
  remove(key) {
    return localStorage.removeItem(key);
  }

}

