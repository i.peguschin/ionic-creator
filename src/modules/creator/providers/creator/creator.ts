import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import { Events } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { ModelCreatorWidgetPlace } from '../../models/CreatorWidgets';
import { HelperProvider } from '../helper/helper';
import DocumentReference = firebase.firestore.DocumentReference;

@Injectable()
export class CreatorProvider {

  user: firebase.User;
  show     = false;
  editMode = false;

  OnSwitchEditMode = new Observable(observe => {
    this._events.subscribe('OnSwitchEditMode', (mode) => observe.next(mode))
  });

  constructor(private _afs: AngularFirestore,
              private _afa: AngularFireAuth,
              private _helperProv: HelperProvider,
              private _events: Events) {

    this._afa.authState.subscribe((user: firebase.User) => {
        this.user = user;

        if (this.user) {
          this.editMode = true;
        }

      }
    );

    let holdKeys      = '';
    let activationKey = 17; //strg
    let timeout       = null;
    document.addEventListener("keydown", (e) => {

      let keyCode = e.keyCode;

      if (keyCode == activationKey) {

        //tab activationKey x 3 to active preview switcher
        if (holdKeys == (
            '' + activationKey + activationKey + activationKey)) {

          this.show = true;

          if (this.user) {
            this.editMode = !this.editMode;
            this._events.publish('OnSwitchEditMode', this.editMode);
          }

          clearTimeout(timeout);
          timeout = setTimeout(() => {
            holdKeys = '';
            console.log('Reset holdKeys');
          }, 3000);

        } else {
          holdKeys += keyCode;
        }

      }

    }, false);


  }

  /**
   *
   * @param res

   */
  private _mapSnapshotChanges(res): Array<{ id: string, data: any }> {
    return res.map(a => {
      const data = a.payload.doc.data();
      const id   = a.payload.doc.id;
      return {
        id  : id,
        data: data
      };
    });
  };

  /**
   *
   * @param widgetPlaceId
   *
   */
  getWidgetPlaceHistory(widgetPlaceId): Observable<Array<ModelCreatorWidgetPlace>> {
    return this._afs.doc('creator/widgets')
      .collection(widgetPlaceId, ref => {
        return ref.orderBy('active', 'desc');
      })
      .snapshotChanges()
      .map(res => this._mapSnapshotChanges(res))
      .map(res => {
        return res.map(data => {
          return new ModelCreatorWidgetPlace(data);
        });
      });
  }

  getActiveWidgetPlace(widgetPlaceId): Observable<ModelCreatorWidgetPlace> {
    return this._afs.doc('creator/widgets')
      .collection(widgetPlaceId, ref => {
        return ref
          .where('active', '<=', new Date())
          .orderBy('active', 'desc')
          .limit(1);
      })
      .snapshotChanges()
      .map(res => this._mapSnapshotChanges(res))
      .map(res => {
        return res.map(data => {
          return new ModelCreatorWidgetPlace(data);
        })[0];
      });

  }

  update(widgetPlaceId, historyId, data = {}) {
    let x = this._afs.doc('creator/widgets/' + widgetPlaceId + '/' + historyId).update(data);
    x.then(() => {
      this._helperProv.toast('Wurde gespeichert!');
    });
    return x;
  }

  add(widgetPlaceId, data): Promise<DocumentReference> {
    return this._afs.doc('creator/widgets/').collection(widgetPlaceId).add(data);
  }

  delete(widgetPlaceId, historyId) {
    return this._afs.doc('creator/widgets/' + widgetPlaceId + '/' + historyId).delete()
  }
}
