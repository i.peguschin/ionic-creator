import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { CreatorWidgetDemoComponent } from '../../components/creator-widget-demo/creator-widget-demo';
import { CreatorWidgetHeadlineComponent } from '../../components/creator-widget-headline/creator-widget-headline';
import { CreatorWidgetImageComponent } from '../../components/creator-widget-image/creator-widget-image';
import { CreatorWidgetParagraphComponent } from '../../components/creator-widget-paragraph/creator-widget-paragraph';
import { CreatorWidgetTableComponent } from '../../components/creator-widget-table/creator-widget-table';
import { InterfaceCreator } from '../../interfaces/creator';
import {
  ModelCreatorWidgetDemo, ModelCreatorWidgetHeadline, ModelCreatorWidgetImage, ModelCreatorWidgetParagraph,
  ModelCreatorWidgetTable
} from '../../models/CreatorWidgets';

@Injectable()
export class CreatorWidgetsProvider {

  widgets = {

    demo: {
      component: CreatorWidgetDemoComponent,
      model    : ModelCreatorWidgetDemo
    },

    headline: {
      component: CreatorWidgetHeadlineComponent,
      model    : ModelCreatorWidgetHeadline
    },

    paragraph: {
      component: CreatorWidgetParagraphComponent,
      model    : ModelCreatorWidgetParagraph
    },

    image: {
      component: CreatorWidgetImageComponent,
      model    : ModelCreatorWidgetImage
    },

    table: {
      component: CreatorWidgetTableComponent,
      model    : ModelCreatorWidgetTable
    }

  };

  modelize(widgets: Array<InterfaceCreator.Widget>) {

    return widgets.filter(widget => !!this.widgets[widget.type]).map(widget => {
      return new this.widgets[widget.type].model(widget);
    });

  }

  constructor(configCustomWidgets) {
    configCustomWidgets = configCustomWidgets || {};
    Object.assign(this.widgets, configCustomWidgets);
    console.log(this.widgets)
  }

}

