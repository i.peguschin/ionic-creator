import { Injectable, NgZone, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Alert, AlertController, App, Content, ToastController, ViewController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

/**
 * Helper provider for various functions
 */
@Injectable()
export class HelperProvider {

  /**
   * Active component content
   */
  @ViewChild(Content) content: Content;

  /**
   *
   * type {Observable<any>}
   */
  OnResize = new Observable(observer => {

    let timeout = null;

    window.addEventListener('resize', (event) => {

      clearTimeout(timeout);

      this._zone.runOutsideAngular(() => {

        timeout = setTimeout(() => {
          observer.next(event);
        }, 100);

      });

    });

    observer.next(event);
  });


  /**
   *
   * @param _toastCtrl
   * @param _translate
   * @param _alertCtrl
   * @param _app
   * @param _zone
   */
  constructor(private _toastCtrl: ToastController, private _translate: TranslateService,
              private _alertCtrl: AlertController, private _app: App, private _zone: NgZone) {
  }

  /**
   *
   * @param message
   * @param position
   * @param translate
   */
  toast(message, position = 'bottom', translate = true) {

    let toast = this._toastCtrl.create({
      message : (translate) ? this._translate.instant(message) : message,
      position: position,
      cssClass: 'text-center'
    });

    toast.present({updateUrl: false});

    // Close after x seconds without update url
    this._zone.runOutsideAngular(() => setTimeout(() => toast.dismiss(null, null, {updateUrl: false}), 3000));
  }

  /**
   *
   * @description Replace special caracters
   * @param s
   *  String mit ersetzten Zeichen
   */
  replaceUmlauts(s) {
    return s.replace(/[äöüÄÖÜßáàâéèêíìîóòôúùûç]/g, ($0) => {
      let tr = {
        'ä': 'ae',
        'ü': 'ue',
        'ö': 'oe',
        'ß': 'ss',
        'Ä': 'AE',
        'Ü': 'UE',
        'Ö': 'OE',
        'á': 'a',
        'à': 'a',
        'â': 'a',
        'é': 'e',
        'è': 'e',
        'ê': 'e',
        'í': 'i',
        'ì': 'i',
        'î': 'i',
        'ó': 'o',
        'ò': 'o',
        'ô': 'o',
        'ú': 'u',
        'ù': 'u',
        'û': 'u',
        'ç': 'c'
      };
      return tr[$0];
    });
  }


  /**
   *
   * @description Überprüft ob die Anwendung unter "Locahost" läuft
   *
   */
  get isDev() {
    return window.location.href.indexOf('localhost:') > -1
  }

  /**
   * Überprüft ob die Variable existiert
   *
   */
  get isCrawler() {
    const botPattern = "(bot|google|baidu|bing|msn|duckduckgo|teoma|slurp|yandex)";
    const re         = new RegExp(botPattern, 'i');

    return window['isCrawler'] || re.test(navigator.userAgent) || false;
  }


  /**
   * Individual alert
   * @param title
   * @param message
   * @param translate
   * @param interpolateParams
   *
   */
  alert(title, message, translate: boolean = true, interpolateParams?: Object): Alert {
    let alert = this._alertCtrl.create({
      title   : (translate) ? this._translate.instant(title, interpolateParams) : title,
      subTitle: (translate) ? this._translate.instant(message, interpolateParams) : message,
      buttons : ['OK']
    });
    alert.present({updateUrl: false});
    return alert;
  }


  /**
   * Confirm
   * @param title
   * @param message
   * @param translate
   *
   */
  confirm(title, message, translate: boolean = true) {
    return new Promise((resolve, reject) => {
      let confirm = this._alertCtrl.create({
        title  : (translate) ? this._translate.instant(title) : title,
        message: (translate) ? this._translate.instant(message) : message,
        buttons: [
          {
            text   : this._translate.instant('GLOBAL.NO'),
            handler: reject
          }, {
            text   : this._translate.instant('GLOBAL.YES'),
            handler: resolve
          }
        ]
      });
      confirm.present();
    });
  }


  /**
   * @description Gibt den aktuellen Unix timestamp zurück
   *  timestamp Timestamp
   */
  get timestamp() {
    return Math.round(new Date().getTime() / 1000);
  }


  /**
   * Find index object in array of objects
   * @param objectNeedle
   * @param ArrayStack
   *
   */
  findIndexOfObjectInArray(objectNeedle: any, ArrayStack: Array<any>) {

    if (!Array.isArray(ArrayStack)) {
      ArrayStack = [];
    }

    return ArrayStack.findIndex(arrayElement => {
      return JSON.stringify(arrayElement) === JSON.stringify(objectNeedle);
    });
  }


  /**
   * Compare objects
   * @param a
   * @param b
   *
   */
  compare(a, b) {

    // // Create arrays of property names
    // var aProps = Object.getOwnPropertyNames(a);
    // var bProps = Object.getOwnPropertyNames(b);
    //
    // // If number of properties is different,
    // // objects are not equivalent
    // if (aProps.length != bProps.length) {
    //   return false;
    // }
    //
    // for (var i = 0; i < aProps.length; i++) {
    //   var propName = aProps[i];
    //
    //   // If values of same property are not equal,
    //   // objects are not equivalent
    //   if (a[propName] !== b[propName]) {
    //     return false;
    //   }
    // }
    //
    // // If we made it this far, objects
    // // are considered equivalent
    // return true;

    return JSON.stringify(a) === JSON.stringify(b);
  }


  /**
   * Generate copy of object without object references
   * @param a
   * @param classObject
   *
   */
  copy(a, classObject?) {

    const data = JSON.parse(JSON.stringify(a));

    if (classObject) {
      return Object.assign(new classObject(...data), a);
    } else {
      return data;
    }

  }

  /**
   * Round number
   * .round(5.2123123, 10) => 5.2
   * .round(5.2123123, 100) => 5.21
   * @param x
   * @param n
   *
   */
  round(x, n) {
    return (Math.round(x * n) / n)
  }

  /**
   * Load src manually
   * @param filePath
   * @param filetype
   *
   */
  loadSrc(filePath: string, filetype: string) {

    return new Promise((resolve) => {

      let fileref;

      if (filetype == "js") { //if filename is a external JavaScript file
        fileref        = document.createElement('script');
        fileref.onload = resolve;
        fileref.setAttribute("type", "text/javascript");
        fileref.setAttribute("src", filePath);
      } else if (filetype == "css") { //if filename is an external CSS file
        fileref = document.createElement("link");
        fileref.setAttribute("rel", "stylesheet");
        fileref.onload = resolve;
        fileref.setAttribute("type", "text/css");
        fileref.setAttribute("href", filePath)
      }

      if (typeof fileref != "undefined") {
        document.getElementsByTagName("head")[0].appendChild(fileref)
      }

    });

  }

  /**
   * Get current view controller
   *
   */
  getCurrentViewController(): ViewController {
    let nav = this._app.getActiveNavs()[0];
    return nav.getActive();
  }

  /**
   * Get current component instance
   *
   */
  getCurrentComponentInstance() {
    return this.getComponentInstance(this.getCurrentViewController());
  }

  /**
   * Get component instance by page
   * @param page
   *
   */
  getComponentInstance(page: ViewController) {
    return page.instance;
  }

  /**
   * Generate random number
   *
   */
  randNum(min = 111111, max = 999999) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  /**
   * Promise stacker
   * Helper method do prevent doubled promises
   * usage: helperService.getPromise('loadData', (resolve, reject) => {resolve()|reject()})
   * @param key
   * @param func
   *
   */
  private _promises         = {};
  private _promisesResolved = {};

  getPromise(key, func): Promise<any> {
    if (this._promises[key] && !this._promisesResolved[key]) {
      return this._promises[key];
    }
    this._promises[key]         = new Promise(func);
    this._promisesResolved[key] = false;
    this._promises[key].then(() => {
      this._promisesResolved[key] = true;
    }).catch(() => {
      this._promisesResolved[key] = true;
    });
    return this._promises[key];
  }

}
