import { Component, ComponentFactoryResolver, EventEmitter, Input, Output, ViewChild, ViewContainerRef } from '@angular/core';
import { PopoverController } from 'ionic-angular';
import { InterfaceCreator } from '../../interfaces/creator';
import { CreatorWidgetsProvider } from '../../providers/creator/creator-widgets';
import { CreatorProvider } from '../../providers/creator/creator';
import { HelperProvider } from '../../providers/helper/helper';

@Component({
  selector   : 'creator-widget',
  templateUrl: 'creator-widget.html'
})
export class CreatorWidgetComponent {

  @ViewChild('widgetPlace', {read: ViewContainerRef}) container: ViewContainerRef;

  @Input() data: InterfaceCreator.Widget;
  @Output() dataChange: EventEmitter<InterfaceCreator.Widget> = new EventEmitter();
  @Output() remove: EventEmitter<any>                         = new EventEmitter();
  @Output() moveDown: EventEmitter<any>                       = new EventEmitter();
  @Output() moveUp: EventEmitter<any>                         = new EventEmitter();
           componentRefInstance;
           dataBackup: InterfaceCreator.Widget;

  constructor(public helperProv: HelperProvider,
              public creatorProv: CreatorProvider,
              private creatorWidgetsProv: CreatorWidgetsProvider,
              private componentFactoryResolver: ComponentFactoryResolver,
              private _popoverCtrl: PopoverController) {
  }

  _loadWidget(widget) {

    if (!widget) {
      return false;
    }

    try {

      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(widget.component);
      let componentRef       = this.container.createComponent(componentFactory);

      this.componentRefInstance = componentRef.instance;

      this.componentRefInstance['widget'] = this.data;

      //cmpRefInstance.someOutput.subscribe(val => doSomething());

      componentRef.changeDetectorRef.detectChanges();

    } catch (e) {
      console.log(e);
    }

  }

  ngOnInit() {
    this.dataBackup = this.helperProv.copy(this.data);
    this._loadWidget(this.creatorWidgetsProv.widgets[this.data.type]);
  }

  revertAction() {
    this.data = this.helperProv.copy(this.dataBackup);
    this.dataChange.emit(this.data);
  }

  openSettings(event) {

    let popover = this._popoverCtrl.create(this.componentRefInstance.settingsComponent, {
      widget: this.data
    });

    popover.present({
      ev: event
    });

  }
}
