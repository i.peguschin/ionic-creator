import { Component, Input, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ModelCreatorWidgetPlace } from '../../models/CreatorWidgets';
import { CreatorProvider } from '../../providers/creator/creator';
import { HelperProvider } from '../../providers/helper/helper';
import { LocalstorageProvider } from '../../providers/localstorage/localstorage';

@Component({
  selector   : 'creator',
  templateUrl: 'creator.html'
})
export class CreatorComponent implements OnDestroy {

  @Input() name: string;
           editMode                                 = false;
           widgetsSubscription$;
           _history: Array<ModelCreatorWidgetPlace> = [];
           historyObserver$: Observable<Array<ModelCreatorWidgetPlace>>;
           newData: ModelCreatorWidgetPlace         = new ModelCreatorWidgetPlace();
           currentActiveContent: ModelCreatorWidgetPlace;
           originalData: ModelCreatorWidgetPlace;
           tmpOriginalData: ModelCreatorWidgetPlace;

  constructor(public creator: CreatorProvider,
              public helperProv: HelperProvider,
              private _localStorageProv: LocalstorageProvider) {

  }

  ngOnInit() {

    this._reset();

    this.historyObserver$ = this.creator.getWidgetPlaceHistory(this.name).map(res => {
      this._history = res;
      return res;
    });

    this.widgetsSubscription$ = this.creator.getActiveWidgetPlace(this.name).subscribe((res) => {

      // Remove old cached content per default
      this._localStorageProv.remove(this.name);

      if (res) {
        this._localStorageProv.set(this.name, res);
        this.currentActiveContent = res;
        this.setData(res);
      } else {
        this._reset();
      }
    });

  }

  ngOnDestroy() {
    if (this.widgetsSubscription$) this.widgetsSubscription$.unsubscribe();
  }

  /**
   * Set data
   * @param data
   */
  setData(data: ModelCreatorWidgetPlace) {
    this.originalData    = data;
    this.tmpOriginalData = this.helperProv.copy(this.originalData);
  }

  /**
   * Select histroy by id
   * @param id
   */
  selectHistory(id) {
    const historyData = this._history.find(entry => entry.id === id);
    this.setData(historyData)
  }

  /**
   * reset defaults

   */
  _reset() {
    let storage = this._localStorageProv.get(this.name);
    let tmp     = storage ? new ModelCreatorWidgetPlace(storage) : new ModelCreatorWidgetPlace();
    this.setData(tmp);
  }

  /**
   * Update actions
   */
  update() {

    this.tmpOriginalData.data.active = new Date(this.tmpOriginalData.data.active);

    this.tmpOriginalData.data.widgets = this.helperProv.copy(this.tmpOriginalData.data.widgets);

    if (this.tmpOriginalData.id) {
      this.creator.update(this.name, this.tmpOriginalData.id, this.tmpOriginalData.data).then(res => {
        this.helperProv.toast('History updated');
        this.selectHistory(this.tmpOriginalData.id);
      });
    } else {
      this.creator.add(this.name, this.tmpOriginalData.data).then(res => {
        this.helperProv.toast('History added');
        this.selectHistory(res.id);
      });
    }

  }

  /**
   * Revert changes
   *
   */
  revertAll() {
    return this.tmpOriginalData = this.helperProv.copy(this.originalData);
  }

  /**
   *
   * @param e1
   * @param e2
   *
   */
  compareFn(e1, e2): boolean {
    return e1 && e2 ? e1.id === e2.id : e1 === e2;
  }

  /**
   * Delete
   */
  deleteHistory() {

    const confirm = window.confirm('Are you sure you want to delete it?');

    if (confirm) {
      this.creator.delete(this.name, this.tmpOriginalData.id).then(res => {
        this._reset();
      });
    }

  }

}
