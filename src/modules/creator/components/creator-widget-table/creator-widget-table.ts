import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { ModelCreatorWidgetTable } from '../../models/CreatorWidgets';
import { CreatorProvider } from '../../providers/creator/creator';


@Component({
  selector   : 'creator-widget-table',
  templateUrl: 'creator-widget-table.html'
})
export class CreatorWidgetTableComponent {

  widget: ModelCreatorWidgetTable;

  settingsComponent = CreatorWidgetTableSettingsComponent;

  constructor(public creatorProv: CreatorProvider) {
  }

  move(from, to) {
    this.widget.cols.splice(to, 0, this.widget.cols.splice(from, 1)[0]);
    return this;
  };

  removeCol(index) {
    this.widget.cols.splice(index, 1);
  }

  moveLeft(index) {
    this.move(index, index - 1);
  }

  moveRight(index) {
    this.move(index, index + 1);
  }

}


@Component({
  templateUrl: 'creator-widget-table-settings.html'
})
export class CreatorWidgetTableSettingsComponent {

  data: ModelCreatorWidgetTable = this.navParams.get('widget');

  constructor(private navParams: NavParams) {
  }

  addCol() {
    this.data.cols.push({widgets: []});
  }
}