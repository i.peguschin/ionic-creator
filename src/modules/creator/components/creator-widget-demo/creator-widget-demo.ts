import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { ModelCreatorWidgetDemo, ModelCreatorWidgetImage } from '../../models/CreatorWidgets';
import { CreatorProvider } from '../../providers/creator/creator';

@Component({
  selector   : 'creator-widget-demo',
  templateUrl: 'creator-widget-demo.html'
})
export class CreatorWidgetDemoComponent {

  widget: ModelCreatorWidgetDemo;

  settingsComponent = CreatorWidgetDemoSettingsComponent;

  constructor(public creatorProv: CreatorProvider) {

  }

}


@Component({
  templateUrl: 'creator-widget-demo-settings.html'
})
export class CreatorWidgetDemoSettingsComponent {

  data: ModelCreatorWidgetImage = this.navParams.get('widget');

  constructor(private navParams: NavParams) {
  }
}