import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { HelperProvider } from '../../providers/helper/helper';

@Component({
  selector   : 'ckeditor',
  templateUrl: 'ckeditor.html'
})
export class CkeditorComponent {

  /**
   * Editor settings
   */
  private _useEditor = 'balloon';

  private _editors = {
    'balloon': 'BalloonEditor',
    'inline' : 'InlineEditor',
    'classic': 'ClassicEditor'
  };

  private _editorConfig = {
    //toolbar      : [],
    //removePlugins: ['Heading'],
  };

  editorInstance;

  @ViewChild('editor') editor: ElementRef;
  @Input() text: string;
  @Output() textChange: EventEmitter<string> = new EventEmitter();

  constructor(private _helperProv: HelperProvider) {
  }

  /**
   *
   *

   */
  _loadScript() {

    return new Promise((resolve) => {

      if (window[this._editors[this._useEditor]]) {
        resolve();
      } else {

        resolve(
          this._helperProv.getPromise('load-ck-editor', (resolve, reject) => {
            console.log('Load ckeditor..');
            this._helperProv.loadSrc('https://cdn.ckeditor.com/ckeditor5/1.0.0-beta.1/' +
              this._useEditor + '/ckeditor.js', 'js')
              .then(resolve).catch(reject);
          })
        );

      }

    });

  }

  /**
   * change event

   */
  _bindChanges = () => {
    this.textChange.emit(this.editorInstance.getData());
  };

  ngOnInit() {
    this._loadScript().then(() => {

      window[this._editors[this._useEditor]].create(this.editor.nativeElement, this._editorConfig).then(editor => {
        this.editorInstance = editor;
        this.editorInstance.model.document.on('change', this._bindChanges);
      }).catch(error => {
        console.error(error);
      });

    });
  }

  ngOnDestroy() {
    if (this.editorInstance) {
      this.editorInstance.model.document.off('change', this._bindChanges);
      this.editorInstance.destroy();
    }
  }


}
