import { Component } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { NavParams, PopoverController, ViewController } from 'ionic-angular';
import { InterfaceCreator } from '../../interfaces/creator';
import { CreatorProvider } from '../../providers/creator/creator';
import { CreatorWidgetsProvider } from '../../providers/creator/creator-widgets';
import { HelperProvider } from '../../providers/helper/helper';


@Component({
  templateUrl: 'creator-tools-widget-add.html'
})
export class CreatorToolsWidgetAddComponent {

  constructor(private _navParams: NavParams, private viewCtrl: ViewController,
              public creatorWidgets: CreatorWidgetsProvider) {
  }

  add(type: string) {

    let widget: Array<InterfaceCreator.Widget> = this._navParams.get('widgets');

    const widgetModels = this.creatorWidgets.modelize([{
      type: type
    }]);

    widget.push(widgetModels[0]);

    this.viewCtrl.dismiss(widget);

  }

}


@Component({
  templateUrl: 'creator-tools-login.html'
})
export class CreatorToolsLoginComponent {

  user = 'i.peguschin@gmail.com';
  pwd  = 'cskyline';

  constructor(private angularFireAuth: AngularFireAuth, private _helper: HelperProvider,
              public creatorProv: CreatorProvider) {
  }

  login() {
    this.angularFireAuth.auth.signInWithEmailAndPassword(this.user, this.pwd).then(() => {
      this._helper.toast('Logged');
    }).catch((e) => {
      this._helper.toast('Logging failed');
    });
  }

}


@Component({
  selector   : 'creator-tools',
  templateUrl: 'creator-tools.html'
})
export class CreatorToolsComponent {

  constructor(public creator: CreatorProvider, private _popoverCtrl: PopoverController) {
  }

  presentPopover(e) {
    e.preventDefault();
    this._popoverCtrl.create(CreatorToolsLoginComponent).present()
  }

}