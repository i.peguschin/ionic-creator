import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { ModelCreatorWidgetHeadline } from '../../models/CreatorWidgets';

@Component({
  selector   : 'creator-widget-headline',
  templateUrl: 'creator-widget-headline.html'
})
export class CreatorWidgetHeadlineComponent {

  widget: ModelCreatorWidgetHeadline;

  settingsComponent = CreatorWidgetHeadlineSettingsComponent;

  constructor() {
  }

}


@Component({
  templateUrl: 'creator-widget-headline-settings.html'
})
export class CreatorWidgetHeadlineSettingsComponent {

  data: ModelCreatorWidgetHeadline = this.navParams.get('widget');

  constructor(private navParams: NavParams) {
  }

}
