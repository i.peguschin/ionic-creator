import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { ModelCreatorWidgetImage } from '../../models/CreatorWidgets';
import { CreatorProvider } from '../../providers/creator/creator';

@Component({
  selector   : 'creator-widget-image',
  templateUrl: 'creator-widget-image.html'
})
export class CreatorWidgetImageComponent {

  widget: ModelCreatorWidgetImage;

  settingsComponent = CreatorWidgetImageSettingsComponent;

  constructor(public creator: CreatorProvider) {
  }

}

@Component({
  templateUrl: 'creator-widget-image-settings.html'
})
export class CreatorWidgetImageSettingsComponent {

  data: ModelCreatorWidgetImage = this.navParams.get('widget');

  constructor(private navParams: NavParams) {
  }
}