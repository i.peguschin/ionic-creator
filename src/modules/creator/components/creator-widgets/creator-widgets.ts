import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PopoverController } from 'ionic-angular';
import { Subject } from 'rxjs/Subject';
import { InterfaceCreator } from '../../interfaces/creator';
import { ModelCreatorWidget } from '../../models/CreatorWidgets';
import { CreatorProvider } from '../../providers/creator/creator';
import { HelperProvider } from '../../providers/helper/helper';
import { CreatorToolsWidgetAddComponent } from '../creator-tools/creator-tools';

@Component({
  selector   : 'creator-widgets',
  templateUrl: 'creator-widgets.html'
})
export class CreatorWidgetsComponent {

  @Input() widgets: Array<ModelCreatorWidget>;
  @Output() widgetsChange: EventEmitter<Array<ModelCreatorWidget>> = new EventEmitter();

  actionLog: Subject<{ action: string, widget: InterfaceCreator.Widget }> = new Subject();

  constructor(private _helperProv: HelperProvider,
              private _popoverCtrl: PopoverController,
              public creatorProv: CreatorProvider) {

  }

  log(event: { type: string, value: any }) {

    const data = {
      action: event.type,
      widget: event.value
    };

    this.actionLog.next(data);

  }

  _emit() {
    this.widgetsChange.emit(this.widgets);
  }

  moveWidget(fromIndex, toIndex) {

    let element = this.widgets[fromIndex];

    this.widgets.splice(fromIndex, 1);
    this.widgets.splice(toIndex, 0, element);

    this._emit();
  }

  removeWidget(index) {
    this.widgets.splice(index, 1);
    this._emit();
  }

  addWidget(event) {

    let popover = this._popoverCtrl.create(CreatorToolsWidgetAddComponent, {
      widgets: this.widgets
    });

    popover.present({
      ev       : event,
      updateUrl: false
    });

    popover.onDidDismiss(res => this._emit());

  }

  exportImport() {

    let data = JSON.parse(window.prompt("Enter export from clipboard: Ctrl+V, Enter", JSON.stringify(this.widgets)));

    if (data) {
      this.widgets = data;
      this._emit();
    }

  }

}
