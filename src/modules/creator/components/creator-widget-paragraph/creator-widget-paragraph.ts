import { Component } from '@angular/core';
import { ModelCreatorWidgetParagraph } from '../../models/CreatorWidgets';
import { CreatorProvider } from '../../providers/creator/creator';

@Component({
  selector   : 'creator-widget-paragraph',
  templateUrl: 'creator-widget-paragraph.html'
})
export class CreatorWidgetParagraphComponent {

  widget: ModelCreatorWidgetParagraph;

  constructor(public creatorProv: CreatorProvider) {

  }


}
