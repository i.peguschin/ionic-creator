/**
 * Firebase interface
 */
declare module InterfaceFirebase {
  /**
   * Firebase settings interface
   */
  export interface Settings {
    /**
     * Log errors identifier
     *
     */
    log_errors: boolean;
    /**
     * Disable <product-delivery-information></product-delivery-information>
     *
     */
    inventur: boolean;
  }

  /**
   * Firebas app interface
   */
  export interface App {
    /**
     * App settings
     *
     */
    settings: Settings
  }

  /**
   * Firebase config interface
   */
  export interface Config {
    /**
     * API key
     *
     */
    apiKey: string,
    /**
     * Authentification domain
     *
     */
    authDomain: string,
    /**
     * Database URL
     *
     */
    databaseURL: string,
    /**
     * Project ID
     *
     */
    projectId: string,
    /**
     * Storage bucket
     *
     */
    storageBucket: string,
    /**
     * Message sender ID
     *
     */
    messagingSenderId: string,
  }
}
