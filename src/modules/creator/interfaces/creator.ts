export declare module InterfaceCreator {

  export interface Widget {
    type: string

    [key: string]: any
  }

}
