import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDnDModule } from '@swimlane/ngx-dnd';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { IonicModule } from 'ionic-angular';
import { CkeditorComponent } from './components/ckeditor/ckeditor';
import {
  CreatorToolsComponent, CreatorToolsLoginComponent,
  CreatorToolsWidgetAddComponent
} from './components/creator-tools/creator-tools';
import {
  CreatorWidgetDemoComponent,
  CreatorWidgetDemoSettingsComponent
} from './components/creator-widget-demo/creator-widget-demo';
import {
  CreatorWidgetHeadlineComponent,
  CreatorWidgetHeadlineSettingsComponent
} from './components/creator-widget-headline/creator-widget-headline';
import {
  CreatorWidgetImageComponent,
  CreatorWidgetImageSettingsComponent
} from './components/creator-widget-image/creator-widget-image';
import { CreatorWidgetParagraphComponent } from './components/creator-widget-paragraph/creator-widget-paragraph';
import {
  CreatorWidgetTableComponent,
  CreatorWidgetTableSettingsComponent
} from './components/creator-widget-table/creator-widget-table';
import { CreatorWidgetComponent } from './components/creator-widget/creator-widget';
import { CreatorWidgetsComponent } from './components/creator-widgets/creator-widgets';
import { CreatorComponent } from './components/creator/creator';
import { CreatorTextDirective } from './directives/creator-text/creator-text';
import { PipesModule } from './pipes/pipes.module';
import { CreatorProvider } from './providers/creator/creator';
import { CreatorWidgetsProvider } from './providers/creator/creator-widgets';
import { HelperProvider } from './providers/helper/helper';
import { LocalstorageProvider } from './providers/localstorage/localstorage';


@NgModule({
  declarations   : [
    CkeditorComponent,
    CreatorTextDirective,
    CreatorComponent,
    CreatorToolsComponent,
    CreatorToolsLoginComponent,
    CreatorToolsWidgetAddComponent,
    CreatorWidgetComponent,
    CreatorWidgetDemoComponent,
    CreatorWidgetDemoSettingsComponent,
    CreatorWidgetImageComponent,
    CreatorWidgetImageSettingsComponent,
    CreatorWidgetTableComponent,
    CreatorWidgetTableSettingsComponent,
    CreatorWidgetParagraphComponent,
    CreatorWidgetHeadlineComponent,
    CreatorWidgetHeadlineSettingsComponent,
    CreatorWidgetsComponent,
  ],
  entryComponents: [
    CreatorToolsLoginComponent,
    CreatorToolsWidgetAddComponent,
    CreatorWidgetDemoComponent,
    CreatorWidgetDemoSettingsComponent,
    CreatorWidgetImageComponent,
    CreatorWidgetImageSettingsComponent,
    CreatorWidgetTableComponent,
    CreatorWidgetTableSettingsComponent,
    CreatorWidgetParagraphComponent,
    CreatorWidgetHeadlineComponent,
    CreatorWidgetHeadlineSettingsComponent,
  ],
  providers      : [
    CreatorProvider,
    HelperProvider,
    LocalstorageProvider
  ],
  imports        : [
    PipesModule,
    TranslateModule.forRoot(),
    HttpClientModule,
    AngularFireModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    IonicModule,
    CommonModule,
    NgxDnDModule
  ],
  exports        : [
    CreatorComponent,
    CreatorToolsComponent
  ]
})
export class CreatorModule {
  static forRoot(config: {
    customWidgets: {
      [key: string]: {
        component: any, model: any
      }
    }
  }) {
    return {
      ngModule : CreatorModule,
      providers: [
        CreatorProvider,
        {
          provide : CreatorWidgetsProvider,
          useValue: new CreatorWidgetsProvider(config.customWidgets)
        }
      ]
    };
  }
}
