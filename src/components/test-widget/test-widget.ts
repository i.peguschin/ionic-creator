import { Component } from '@angular/core';
import { CreatorProvider, ModelCreatorWidget } from '../../../ionic-creator';

@Component({
  selector   : 'test-widget',
  templateUrl: 'test-widget.html'
})
export class TestWidgetComponent {

  widget: ModelCustomWidget;

  constructor(public creator: CreatorProvider) {
  }

}

export class ModelCustomWidget extends ModelCreatorWidget {

  constructor(widget) {
    super(widget);
  }

}
