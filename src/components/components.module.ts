import { NgModule } from '@angular/core';
import { TestWidgetComponent } from './test-widget/test-widget';
@NgModule({
	declarations: [TestWidgetComponent],
	entryComponents: [
    TestWidgetComponent
	],
	imports: [],
	exports: [TestWidgetComponent]
})
export class ComponentsModule {}
