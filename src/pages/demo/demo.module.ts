import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from '../../components/components.module';
import { DemoPage } from './demo';

@NgModule({
  declarations: [
    DemoPage,
  ],
  imports     : [
    ComponentsModule,
    IonicPageModule.forChild(DemoPage),
  ],
})
export class DemoPageModule {
}
