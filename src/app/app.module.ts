import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { AngularFireModule } from 'angularfire2';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { CreatorModule } from '../../ionic-creator';
import { ComponentsModule } from '../components/components.module';
import { ModelCustomWidget, TestWidgetComponent } from '../components/test-widget/test-widget';
import { HomePage } from '../pages/home/home';
import { MyApp } from './app.component';


/**
 * Must export the config
 */
const firebaseConfig = {
  apiKey           : "AIzaSyAfbrPWa_LXTOjXFpggCSRK6CTIQSavZ8Y",
  authDomain       : "angular-17618.firebaseapp.com",
  databaseURL      : "https://angular-17618.firebaseio.com",
  projectId        : "angular-17618",
  storageBucket    : "",
  messagingSenderId: "945848960651"
};

@NgModule({
  declarations   : [
    MyApp,
    HomePage
  ],
  imports        : [
    ComponentsModule,
    BrowserModule,

    /**
     * Important for creator
     */
    AngularFireModule.initializeApp(firebaseConfig),
    CreatorModule.forRoot({
      customWidgets: {
        test: {
          component: TestWidgetComponent,
          model    : ModelCustomWidget
        }
      }
    }),
    /** ENDE **/

    IonicModule.forRoot(MyApp)
  ],
  bootstrap      : [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers      : [
    StatusBar,
    SplashScreen,
    {
      provide : ErrorHandler,
      useClass: IonicErrorHandler
    }
  ]
})
export class AppModule {
}
