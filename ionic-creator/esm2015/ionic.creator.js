import { Injectable, NgZone, ViewChild, Component, ElementRef, EventEmitter, Input, Output, ComponentFactoryResolver, ViewContainerRef, Directive, forwardRef, Pipe, NgModule } from '@angular/core';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { AlertController, App, Content, ToastController, Events, NavParams, PopoverController, ViewController, IonicModule } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth, AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreModule } from 'angularfire2/firestore';
import { Subject } from 'rxjs/Subject';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgxDnDModule } from '@swimlane/ngx-dnd';
import { AngularFireModule } from 'angularfire2';

class HelperProvider {
    constructor(_toastCtrl, _translate, _alertCtrl, _app, _zone) {
        this._toastCtrl = _toastCtrl;
        this._translate = _translate;
        this._alertCtrl = _alertCtrl;
        this._app = _app;
        this._zone = _zone;
        this.OnResize = new Observable(observer => {
            let                  timeout = null;
            window.addEventListener('resize', (event) => {
                clearTimeout(timeout);
                this._zone.runOutsideAngular(() => {
                    timeout = setTimeout(() => {
                        observer.next(event);
                    }, 100);
                });
            });
            observer.next(event);
        });
        this._promises = {};
        this._promisesResolved = {};
    }
    toast(message, position = 'bottom', translate = true) {
        let                  toast = this._toastCtrl.create({
            message: (translate) ? this._translate.instant(message) : message,
            position: position,
            cssClass: 'text-center'
        });
        toast.present({ updateUrl: false });
        this._zone.runOutsideAngular(() => setTimeout(() => toast.dismiss(null, null, { updateUrl: false }), 3000));
    }
    replaceUmlauts(s) {
        return s.replace(/[äöüÄÖÜßáàâéèêíìîóòôúùûç]/g, ($0) => {
            let                  tr = {
                'ä': 'ae',
                'ü': 'ue',
                'ö': 'oe',
                'ß': 'ss',
                'Ä': 'AE',
                'Ü': 'UE',
                'Ö': 'OE',
                'á': 'a',
                'à': 'a',
                'â': 'a',
                'é': 'e',
                'è': 'e',
                'ê': 'e',
                'í': 'i',
                'ì': 'i',
                'î': 'i',
                'ó': 'o',
                'ò': 'o',
                'ô': 'o',
                'ú': 'u',
                'ù': 'u',
                'û': 'u',
                'ç': 'c'
            };
            return tr[$0];
        });
    }
    get isDev() {
        return window.location.href.indexOf('localhost:') > -1;
    }
    get isCrawler() {
        const                  botPattern = "(bot|google|baidu|bing|msn|duckduckgo|teoma|slurp|yandex)";
        const                  re = new RegExp(botPattern, 'i');
        return window['isCrawler'] || re.test(navigator.userAgent) || false;
    }
    alert(title, message, translate = true, interpolateParams) {
        let                  alert = this._alertCtrl.create({
            title: (translate) ? this._translate.instant(title, interpolateParams) : title,
            subTitle: (translate) ? this._translate.instant(message, interpolateParams) : message,
            buttons: ['OK']
        });
        alert.present({ updateUrl: false });
        return alert;
    }
    confirm(title, message, translate = true) {
        return new Promise((resolve, reject) => {
            let                  confirm = this._alertCtrl.create({
                title: (translate) ? this._translate.instant(title) : title,
                message: (translate) ? this._translate.instant(message) : message,
                buttons: [
                    {
                        text: this._translate.instant('GLOBAL.NO'),
                        handler: reject
                    }, {
                        text: this._translate.instant('GLOBAL.YES'),
                        handler: resolve
                    }
                ]
            });
            confirm.present();
        });
    }
    get timestamp() {
        return Math.round(new Date().getTime() / 1000);
    }
    findIndexOfObjectInArray(objectNeedle, ArrayStack) {
        if (!Array.isArray(ArrayStack)) {
            ArrayStack = [];
        }
        return ArrayStack.findIndex(arrayElement => {
            return JSON.stringify(arrayElement) === JSON.stringify(objectNeedle);
        });
    }
    compare(a, b) {
        return JSON.stringify(a) === JSON.stringify(b);
    }
    copy(a, classObject) {
        const                  data = JSON.parse(JSON.stringify(a));
        if (classObject) {
            return Object.assign(new classObject(...data), a);
        }
        else {
            return data;
        }
    }
    round(x, n) {
        return (Math.round(x * n) / n);
    }
    loadSrc(filePath, filetype) {
        return new Promise((resolve) => {
            let                  fileref;
            if (filetype == "js") {
                fileref = document.createElement('script');
                fileref.onload = resolve;
                fileref.setAttribute("type", "text/javascript");
                fileref.setAttribute("src", filePath);
            }
            else if (filetype == "css") {
                fileref = document.createElement("link");
                fileref.setAttribute("rel", "stylesheet");
                fileref.onload = resolve;
                fileref.setAttribute("type", "text/css");
                fileref.setAttribute("href", filePath);
            }
            if (typeof fileref != "undefined") {
                document.getElementsByTagName("head")[0].appendChild(fileref);
            }
        });
    }
    getCurrentViewController() {
        let                  nav = this._app.getActiveNavs()[0];
        return nav.getActive();
    }
    getCurrentComponentInstance() {
        return this.getComponentInstance(this.getCurrentViewController());
    }
    getComponentInstance(page) {
        return page.instance;
    }
    randNum(min = 111111, max = 999999) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }
    getPromise(key, func) {
        if (this._promises[key] && !this._promisesResolved[key]) {
            return this._promises[key];
        }
        this._promises[key] = new Promise(func);
        this._promisesResolved[key] = false;
        this._promises[key].then(() => {
            this._promisesResolved[key] = true;
        }).catch(() => {
            this._promisesResolved[key] = true;
        });
        return this._promises[key];
    }
}
HelperProvider.decorators = [
    { type: Injectable },
];
HelperProvider.ctorParameters = () => [
    { type: ToastController, },
    { type: TranslateService, },
    { type: AlertController, },
    { type: App, },
    { type: NgZone, },
];
HelperProvider.propDecorators = {
    "content": [{ type: ViewChild, args: [Content,] },],
};

class CkeditorComponent {
    constructor(_helperProv) {
        this._helperProv = _helperProv;
        this._useEditor = 'balloon';
        this._editors = {
            'balloon': 'BalloonEditor',
            'inline': 'InlineEditor',
            'classic': 'ClassicEditor'
        };
        this._editorConfig = {};
        this.textChange = new EventEmitter();
        this._bindChanges = () => {
            this.textChange.emit(this.editorInstance.getData());
        };
    }
    _loadScript() {
        return new Promise((resolve) => {
            if (window[this._editors[this._useEditor]]) {
                resolve();
            }
            else {
                resolve(this._helperProv.getPromise('load-ck-editor', (resolve, reject) => {
                    console.log('Load ckeditor..');
                    this._helperProv.loadSrc('https://cdn.ckeditor.com/ckeditor5/1.0.0-beta.1/' +
                        this._useEditor + '/ckeditor.js', 'js')
                        .then(resolve).catch(reject);
                }));
            }
        });
    }
    ngOnInit() {
        this._loadScript().then(() => {
            window[this._editors[this._useEditor]].create(this.editor.nativeElement, this._editorConfig).then(editor => {
                this.editorInstance = editor;
                this.editorInstance.model.document.on('change', this._bindChanges);
            }).catch(error => {
                console.error(error);
            });
        });
    }
    ngOnDestroy() {
        if (this.editorInstance) {
            this.editorInstance.model.document.off('change', this._bindChanges);
            this.editorInstance.destroy();
        }
    }
}
CkeditorComponent.decorators = [
    { type: Component, args: [{
                selector: 'ckeditor',
                template: `<div #editor [innerHtml]="text"></div>`
            },] },
];
CkeditorComponent.ctorParameters = () => [
    { type: HelperProvider, },
];
CkeditorComponent.propDecorators = {
    "editor": [{ type: ViewChild, args: ['editor',] },],
    "text": [{ type: Input },],
    "textChange": [{ type: Output },],
};

class ModelCreatorWidget {
    constructor(widget) {
        this.type = widget.type;
    }
}
class ModelCreatorWidgetHeadline extends ModelCreatorWidget {
    constructor(widget) {
        super(widget);
        this.text = widget.text || 'This is a headline!';
        this.headline = widget.headline || 1;
    }
}
class ModelCreatorWidgetParagraph extends ModelCreatorWidget {
    constructor(widget) {
        super(widget);
        this.text = widget.text || '<p>Do you see any Teletubbies in here? Do you see a slender plastic tag clipped to my shirt with my name printed on it? Do you see a little Asian child with a blank expression on his face sitting outside on a mechanical helicopter that shakes when you put quarters in it? No? Well, that\'s what you see at a toy store. And you must think you\'re in a toy store, because you\'re here shopping for an infant named Jeb.</p>';
    }
}
class ModelCreatorWidgetImage extends ModelCreatorWidget {
    constructor(widget) {
        super(widget);
        this.src = widget.src || 'http://via.placeholder.com/350x150';
        this.link = widget.link;
        this.linkType = widget.linkType;
    }
}
class ModelCreatorWidgetDemo extends ModelCreatorWidget {
    constructor(widget) {
        super(widget);
        this.foo = widget.foo;
        this.bar = widget.bar || 1;
    }
}
class ModelCreatorWidgetTable extends ModelCreatorWidget {
    constructor(widget) {
        super(widget);
        this.cols = widget["cols"] || [{ widgets: [] }, { widgets: [] }];
        this.cols = this.cols.map(col => {
            col.widgets = col.widgets || [];
            return col;
        });
    }
}
class ModelCreatorWidgetPlace {
    constructor(place = {
            id: null,
            data: {}
        }) {
        this.id = place.id;
        this.data = place.data;
        this.data.active = new Date(this.data.active);
        this.data.name = this.data.name || '';
        this.data.widgets = this.data.widgets || [];
    }
}

class CreatorProvider {
    constructor(_afs, _afa, _helperProv, _events) {
        this._afs = _afs;
        this._afa = _afa;
        this._helperProv = _helperProv;
        this._events = _events;
        this.show = false;
        this.editMode = false;
        this.OnSwitchEditMode = new Observable(observe => {
            this._events.subscribe('OnSwitchEditMode', (mode) => observe.next(mode));
        });
        this._afa.authState.subscribe((user) => {
            this.user = user;
            if (this.user) {
                this.editMode = true;
            }
        });
        let                  holdKeys = '';
        let                  activationKey = 17;
        let                  timeout = null;
        document.addEventListener("keydown", (e) => {
            let                  keyCode = e.keyCode;
            if (keyCode == activationKey) {
                if (holdKeys == ('' + activationKey + activationKey + activationKey)) {
                    this.show = true;
                    if (this.user) {
                        this.editMode = !this.editMode;
                        this._events.publish('OnSwitchEditMode', this.editMode);
                    }
                    clearTimeout(timeout);
                    timeout = setTimeout(() => {
                        holdKeys = '';
                        console.log('Reset holdKeys');
                    }, 3000);
                }
                else {
                    holdKeys += keyCode;
                }
            }
        }, false);
    }
    _mapSnapshotChanges(res) {
        return res.map(a => {
            const                  data = a.payload.doc.data();
            const                  id = a.payload.doc.id;
            return {
                id: id,
                data: data
            };
        });
    }
    ;
    getWidgetPlaceHistory(widgetPlaceId) {
        return this._afs.doc('creator/widgets')
            .collection(widgetPlaceId, ref => {
            return ref.orderBy('active', 'desc');
        })
            .snapshotChanges()
            .map(res => this._mapSnapshotChanges(res))
            .map(res => {
            return res.map(data => {
                return new ModelCreatorWidgetPlace(data);
            });
        });
    }
    getActiveWidgetPlace(widgetPlaceId) {
        return this._afs.doc('creator/widgets')
            .collection(widgetPlaceId, ref => {
            return ref
                .where('active', '<=', new Date())
                .orderBy('active', 'desc')
                .limit(1);
        })
            .snapshotChanges()
            .map(res => this._mapSnapshotChanges(res))
            .map(res => {
            return res.map(data => {
                return new ModelCreatorWidgetPlace(data);
            })[0];
        });
    }
    update(widgetPlaceId, historyId, data = {}) {
        let                  x = this._afs.doc('creator/widgets/' + widgetPlaceId + '/' + historyId).update(data);
        x.then(() => {
            this._helperProv.toast('Wurde gespeichert!');
        });
        return x;
    }
    add(widgetPlaceId, data) {
        return this._afs.doc('creator/widgets/').collection(widgetPlaceId).add(data);
    }
    delete(widgetPlaceId, historyId) {
        return this._afs.doc('creator/widgets/' + widgetPlaceId + '/' + historyId).delete();
    }
}
CreatorProvider.decorators = [
    { type: Injectable },
];
CreatorProvider.ctorParameters = () => [
    { type: AngularFirestore, },
    { type: AngularFireAuth, },
    { type: HelperProvider, },
    { type: Events, },
];

class CreatorWidgetDemoComponent {
    constructor(creatorProv) {
        this.creatorProv = creatorProv;
        this.settingsComponent = CreatorWidgetDemoSettingsComponent;
    }
}
CreatorWidgetDemoComponent.decorators = [
    { type: Component, args: [{
                selector: 'creator-widget-demo',
                template: `<pre>
Editable: {{creatorProv.editMode}}
Widget:  {{ widget | json }}
</pre>
`
            },] },
];
CreatorWidgetDemoComponent.ctorParameters = () => [
    { type: CreatorProvider, },
];
class CreatorWidgetDemoSettingsComponent {
    constructor(navParams) {
        this.navParams = navParams;
        this.data = this.navParams.get('widget');
    }
}
CreatorWidgetDemoSettingsComponent.decorators = [
    { type: Component, args: [{
                template: `<ion-list>
  <ion-item>
    <ion-label stacked>DEMO #1</ion-label>
    <ion-input type="text" name="foo" [(ngModel)]="data.foo"></ion-input>
  </ion-item>
  <ion-item>
    <ion-label>Demo #2</ion-label>
    <select name="bar" [(ngModel)]="data.bar">
      <option value="1">Setting 1</option>
      <option value="2">Setting 2</option>
    </select>
  </ion-item>
</ion-list>
`
            },] },
];
CreatorWidgetDemoSettingsComponent.ctorParameters = () => [
    { type: NavParams, },
];

class CreatorWidgetHeadlineComponent {
    constructor() {
        this.settingsComponent = CreatorWidgetHeadlineSettingsComponent;
    }
}
CreatorWidgetHeadlineComponent.decorators = [
    { type: Component, args: [{
                selector: 'creator-widget-headline',
                template: `<h1 *ngIf="widget.headline === 1" creator-text [(ngModel)]="widget.text"></h1>
<h2 *ngIf="widget.headline === 2" creator-text [(ngModel)]="widget.text"></h2>
<h3 *ngIf="widget.headline === 3" creator-text [(ngModel)]="widget.text"></h3>
<h4 *ngIf="widget.headline === 4" creator-text [(ngModel)]="widget.text"></h4>
<h5 *ngIf="widget.headline === 5" creator-text [(ngModel)]="widget.text"></h5>
<h6 *ngIf="widget.headline === 6" creator-text [(ngModel)]="widget.text"></h6>
`
            },] },
];
CreatorWidgetHeadlineComponent.ctorParameters = () => [];
class CreatorWidgetHeadlineSettingsComponent {
    constructor(navParams) {
        this.navParams = navParams;
        this.data = this.navParams.get('widget');
    }
}
CreatorWidgetHeadlineSettingsComponent.decorators = [
    { type: Component, args: [{
                template: `<ion-list>
  <ion-label>Headline size</ion-label>
  <select name="headline" [(ngModel)]="data.headline">
    <option [value]="h" *ngFor="let h of [1,2,3,4,5,6]">H{{h}}</option>
  </select>
</ion-list>
`
            },] },
];
CreatorWidgetHeadlineSettingsComponent.ctorParameters = () => [
    { type: NavParams, },
];

class CreatorWidgetImageComponent {
    constructor(creator) {
        this.creator = creator;
        this.settingsComponent = CreatorWidgetImageSettingsComponent;
    }
}
CreatorWidgetImageComponent.decorators = [
    { type: Component, args: [{
                selector: 'creator-widget-image',
                template: `<div *ngIf="widget.link" [ngSwitch]="widget.linkType">
  <a *ngSwitchCase="'extern'" [href]="widget.link" target="_blank">
    <img [src]="widget.src">
  </a>
  <a *ngSwitchDefault [href]="widget.link">
    <img [src]="widget.src">
  </a>
</div>
<img *ngIf="!widget.link" [src]="widget.src">
`
            },] },
];
CreatorWidgetImageComponent.ctorParameters = () => [
    { type: CreatorProvider, },
];
class CreatorWidgetImageSettingsComponent {
    constructor(navParams) {
        this.navParams = navParams;
        this.data = this.navParams.get('widget');
    }
}
CreatorWidgetImageSettingsComponent.decorators = [
    { type: Component, args: [{
                template: `<ion-list>
  <ion-item>
    <ion-label stacked>Bild Quelle</ion-label>
    <ion-input type="text" name="src" [(ngModel)]="data.src"></ion-input>
  </ion-item>
  <ion-item>
    <ion-label stacked>Link</ion-label>
    <ion-input type="text" name="link" [(ngModel)]="data.link"></ion-input>
  </ion-item>
  <ion-item>
    <ion-label>Link Typ</ion-label>
    <select name="linkType" [(ngModel)]="data.linkType">
      <option value="intern" [selected]="!data.linkType">intern</option>
      <option value="extern">extern</option>
    </select>
  </ion-item>
</ion-list>
`
            },] },
];
CreatorWidgetImageSettingsComponent.ctorParameters = () => [
    { type: NavParams, },
];

class CreatorWidgetParagraphComponent {
    constructor(creatorProv) {
        this.creatorProv = creatorProv;
    }
}
CreatorWidgetParagraphComponent.decorators = [
    { type: Component, args: [{
                selector: 'creator-widget-paragraph',
                template: `<div *ngIf="!creatorProv.editMode" [innerHtml]="widget.text"></div>
<ckeditor *ngIf="creatorProv.editMode" [(text)]="widget.text"></ckeditor>
`
            },] },
];
CreatorWidgetParagraphComponent.ctorParameters = () => [
    { type: CreatorProvider, },
];

class CreatorWidgetTableComponent {
    constructor(creatorProv) {
        this.creatorProv = creatorProv;
        this.settingsComponent = CreatorWidgetTableSettingsComponent;
    }
    move(from, to) {
        this.widget.cols.splice(to, 0, this.widget.cols.splice(from, 1)[0]);
        return this;
    }
    ;
    removeCol(index) {
        this.widget.cols.splice(index, 1);
    }
    moveLeft(index) {
        this.move(index, index - 1);
    }
    moveRight(index) {
        this.move(index, index + 1);
    }
}
CreatorWidgetTableComponent.decorators = [
    { type: Component, args: [{
                selector: 'creator-widget-table',
                template: `<ul class="table">
  <li *ngFor="let col of widget.cols; let i = index">
    <div text-center *ngIf="creatorProv.editMode" text-nowrap>
      <button small clear ion-button color="secondary" (click)="moveLeft(i)" title="Move col left">⇦</button>
      <button small clear ion-button color="secondary" (click)="removeCol(i)" title="Remove col">⊗</button>
      <button small clear ion-button color="secondary" (click)="moveRight(i)" title="Move col right">⇨</button>
    </div>
    <creator-widgets [(widgets)]="widget.cols[i].widgets"></creator-widgets>
  </li>
</ul>`
            },] },
];
CreatorWidgetTableComponent.ctorParameters = () => [
    { type: CreatorProvider, },
];
class CreatorWidgetTableSettingsComponent {
    constructor(navParams) {
        this.navParams = navParams;
        this.data = this.navParams.get('widget');
    }
    addCol() {
        this.data.cols.push({ widgets: [] });
    }
}
CreatorWidgetTableSettingsComponent.decorators = [
    { type: Component, args: [{
                template: `<ion-list no-margin>
  <button ion-item (click)="addCol()">Spalte hinzufügen</button>
</ion-list>
`
            },] },
];
CreatorWidgetTableSettingsComponent.ctorParameters = () => [
    { type: NavParams, },
];

class CreatorWidgetsProvider {
    constructor(configCustomWidgets) {
        this.widgets = {
            demo: {
                component: CreatorWidgetDemoComponent,
                model: ModelCreatorWidgetDemo
            },
            headline: {
                component: CreatorWidgetHeadlineComponent,
                model: ModelCreatorWidgetHeadline
            },
            paragraph: {
                component: CreatorWidgetParagraphComponent,
                model: ModelCreatorWidgetParagraph
            },
            image: {
                component: CreatorWidgetImageComponent,
                model: ModelCreatorWidgetImage
            },
            table: {
                component: CreatorWidgetTableComponent,
                model: ModelCreatorWidgetTable
            }
        };
        configCustomWidgets = configCustomWidgets || {};
        Object.assign(this.widgets, configCustomWidgets);
        console.log(this.widgets);
    }
    modelize(widgets) {
        return widgets.filter(widget => !!this.widgets[widget.type]).map(widget => {
            return new this.widgets[widget.type].model(widget);
        });
    }
}
CreatorWidgetsProvider.decorators = [
    { type: Injectable },
];
CreatorWidgetsProvider.ctorParameters = () => [
    null,
];

class CreatorToolsWidgetAddComponent {
    constructor(_navParams, viewCtrl, creatorWidgets) {
        this._navParams = _navParams;
        this.viewCtrl = viewCtrl;
        this.creatorWidgets = creatorWidgets;
    }
    add(type) {
        let                  widget = this._navParams.get('widgets');
        const                  widgetModels = this.creatorWidgets.modelize([{
                type: type
            }]);
        widget.push(widgetModels[0]);
        this.viewCtrl.dismiss(widget);
    }
}
CreatorToolsWidgetAddComponent.decorators = [
    { type: Component, args: [{
                template: `<ion-list>
  <button ion-item (click)="add(widget.key)" *ngFor="let widget of creatorWidgets.widgets | keys">
    {{widget.key}}
  </button>
</ion-list>`
            },] },
];
CreatorToolsWidgetAddComponent.ctorParameters = () => [
    { type: NavParams, },
    { type: ViewController, },
    { type: CreatorWidgetsProvider, },
];
class CreatorToolsLoginComponent {
    constructor(angularFireAuth, _helper, creatorProv) {
        this.angularFireAuth = angularFireAuth;
        this._helper = _helper;
        this.creatorProv = creatorProv;
        this.user = 'i.peguschin@gmail.com';
        this.pwd = 'cskyline';
    }
    login() {
        this.angularFireAuth.auth.signInWithEmailAndPassword(this.user, this.pwd).then(() => {
            this._helper.toast('Logged');
        }).catch((e) => {
            this._helper.toast('Logging failed');
        });
    }
}
CreatorToolsLoginComponent.decorators = [
    { type: Component, args: [{
                template: `<form #loginForm="ngForm" (submit)="login()" margin-bottom *ngIf="!creatorProv.user?.uid; else logged">
  <ion-list no-margin>
    <ion-item>
      <ion-label stacked translate="USER.EMAIL.TITLE" class="required"></ion-label>
      <ion-input type="email" email required name="user" [(ngModel)]="user"></ion-input>
    </ion-item>
    <!--<input-valid [input]="userNameModel"></input-valid>-->
    <ion-item>
      <ion-label stacked translate="USER.PWD.TITLE" class="required"></ion-label>
      <ion-input type="password" required name="pwd" [(ngModel)]="pwd"></ion-input>
    </ion-item>
    <!--<input-valid [input]="userPwdModel"></input-valid>-->
  </ion-list>
  <div padding-horizontal>
    <button ion-button block [disabled]="!loginForm.valid">{{ 'GLOBAL.LOGIN' | translate }}</button>
  </div>
</form>
<ng-template #logged>
  <div text-center margin>
    Already logged
    <strong block>{{creatorProv.user?.uid}}</strong>
  </div>
</ng-template>
`
            },] },
];
CreatorToolsLoginComponent.ctorParameters = () => [
    { type: AngularFireAuth, },
    { type: HelperProvider, },
    { type: CreatorProvider, },
];
class CreatorToolsComponent {
    constructor(creator, _popoverCtrl) {
        this.creator = creator;
        this._popoverCtrl = _popoverCtrl;
    }
    presentPopover(e) {
        e.preventDefault();
        this._popoverCtrl.create(CreatorToolsLoginComponent).present();
    }
}
CreatorToolsComponent.decorators = [
    { type: Component, args: [{
                selector: 'creator-tools',
                template: `<ion-fab left bottom *ngIf="creator.show">
  <button ion-fab (click)="presentPopover($event)"></button>
</ion-fab>`
            },] },
];
CreatorToolsComponent.ctorParameters = () => [
    { type: CreatorProvider, },
    { type: PopoverController, },
];

class CreatorWidgetComponent {
    constructor(helperProv, creatorProv, creatorWidgetsProv, componentFactoryResolver, _popoverCtrl) {
        this.helperProv = helperProv;
        this.creatorProv = creatorProv;
        this.creatorWidgetsProv = creatorWidgetsProv;
        this.componentFactoryResolver = componentFactoryResolver;
        this._popoverCtrl = _popoverCtrl;
        this.dataChange = new EventEmitter();
        this.remove = new EventEmitter();
        this.moveDown = new EventEmitter();
        this.moveUp = new EventEmitter();
    }
    _loadWidget(widget) {
        if (!widget) {
            return false;
        }
        try {
            const                  componentFactory = this.componentFactoryResolver.resolveComponentFactory(widget.component);
            let                  componentRef = this.container.createComponent(componentFactory);
            this.componentRefInstance = componentRef.instance;
            this.componentRefInstance['widget'] = this.data;
            componentRef.changeDetectorRef.detectChanges();
        }
        catch (                 e) {
            console.log(e);
        }
    }
    ngOnInit() {
        this.dataBackup = this.helperProv.copy(this.data);
        this._loadWidget(this.creatorWidgetsProv.widgets[this.data.type]);
    }
    revertAction() {
        this.data = this.helperProv.copy(this.dataBackup);
        this.dataChange.emit(this.data);
    }
    openSettings(event) {
        let                  popover = this._popoverCtrl.create(this.componentRefInstance.settingsComponent, {
            widget: this.data
        });
        popover.present({
            ev: event
        });
    }
}
CreatorWidgetComponent.decorators = [
    { type: Component, args: [{
                selector: 'creator-widget',
                template: `<ion-toolbar *ngIf="creatorProv.editMode">
  <!-- <ion-buttons left>
     <button  ion-button ngxDragHandle title="Sort">
     ✥
     </button>
   </ion-buttons>-->
  <ion-title text-uppercase ngxDragHandle>{{data.type}}</ion-title>
  <ion-buttons end>
    <button ion-button *ngIf="componentRefInstance?.settingsComponent" (click)="openSettings($event)" title="Settings">
      ✎
    </button>
    <button ion-button
            [disabled]="helperProv.compare(dataBackup, data)"
            (click)="revertAction()"
            title="Revert changes">↺
    </button>
    <button ion-button (click)="moveUp.emit()" title="Move up">
      ⇧
    </button>
    <button ion-button (click)="moveDown.emit()" title="Move down">
      ⇩
    </button>
    <button ion-button (click)="remove.emit()" title="Remove">
      ✘
    </button>
  </ion-buttons>
</ion-toolbar>
<ng-container #widgetPlace></ng-container>`
            },] },
];
CreatorWidgetComponent.ctorParameters = () => [
    { type: HelperProvider, },
    { type: CreatorProvider, },
    { type: CreatorWidgetsProvider, },
    { type: ComponentFactoryResolver, },
    { type: PopoverController, },
];
CreatorWidgetComponent.propDecorators = {
    "container": [{ type: ViewChild, args: ['widgetPlace', { read: ViewContainerRef },] },],
    "data": [{ type: Input },],
    "dataChange": [{ type: Output },],
    "remove": [{ type: Output },],
    "moveDown": [{ type: Output },],
    "moveUp": [{ type: Output },],
};

class CreatorWidgetsComponent {
    constructor(_helperProv, _popoverCtrl, creatorProv) {
        this._helperProv = _helperProv;
        this._popoverCtrl = _popoverCtrl;
        this.creatorProv = creatorProv;
        this.widgetsChange = new EventEmitter();
        this.actionLog = new Subject();
    }
    log(event) {
        const                  data = {
            action: event.type,
            widget: event.value
        };
        this.actionLog.next(data);
    }
    _emit() {
        this.widgetsChange.emit(this.widgets);
    }
    moveWidget(fromIndex, toIndex) {
        let                  element = this.widgets[fromIndex];
        this.widgets.splice(fromIndex, 1);
        this.widgets.splice(toIndex, 0, element);
        this._emit();
    }
    removeWidget(index) {
        this.widgets.splice(index, 1);
        this._emit();
    }
    addWidget(event) {
        let                  popover = this._popoverCtrl.create(CreatorToolsWidgetAddComponent, {
            widgets: this.widgets
        });
        popover.present({
            ev: event,
            updateUrl: false
        });
        popover.onDidDismiss(res => this._emit());
    }
    exportImport() {
        let                  data = JSON.parse(window.prompt("Enter export from clipboard: Ctrl+V, Enter", JSON.stringify(this.widgets)));
        if (data) {
            this.widgets = data;
            this._emit();
        }
    }
}
CreatorWidgetsComponent.decorators = [
    { type: Component, args: [{
                selector: 'creator-widgets',
                template: `<div class="ngx-dnd-container" ngxDroppable="widgets" [ngClass]="{'gu-empty': !widgets.length}" [model]="widgets"
     (drag)="log($event)"
     (drop)="log($event)"
     (over)="log($event)"
     (out)="log($event)"
     (remove)="log($event)">
  <creator-widget ngxDraggable class="ngx-dnd-item has-handle"
                  *ngFor="let widget of widgets; let i = index"
                  [model]="widgets[i]"
                  [(data)]="widgets[i]"
                  (remove)="removeWidget(i)"
                  (moveDown)="moveWidget(i, i+1)"
                  (moveUp)="moveWidget(i, i-1)"></creator-widget>
</div>
<ion-toolbar color="dark" *ngIf="creatorProv.editMode">
  <ion-buttons left>
    <button ion-button (click)="exportImport($event)" title="Export/Import">
      &#123; &#125;
    </button>
  </ion-buttons>
  <!--<ion-title text-uppercase>{{}}</ion-title>-->
  <ion-buttons end>
    <button ion-button (click)="addWidget($event)">
      ⊕
    </button>
  </ion-buttons>
</ion-toolbar>
`
            },] },
];
CreatorWidgetsComponent.ctorParameters = () => [
    { type: HelperProvider, },
    { type: PopoverController, },
    { type: CreatorProvider, },
];
CreatorWidgetsComponent.propDecorators = {
    "widgets": [{ type: Input },],
    "widgetsChange": [{ type: Output },],
};

class LocalstorageProvider {
    get(key, toJSON = true) {
        let                  c = localStorage.getItem(key);
        return (toJSON && c) ? JSON.parse(c) : localStorage.getItem(key);
    }
    set(key, data, toString = true) {
        return (toString) ? localStorage.setItem(key, JSON.stringify(data)) : localStorage.setItem(key, data);
    }
    remove(key) {
        return localStorage.removeItem(key);
    }
}
LocalstorageProvider.decorators = [
    { type: Injectable },
];

class CreatorComponent {
    constructor(creator, helperProv, _localStorageProv) {
        this.creator = creator;
        this.helperProv = helperProv;
        this._localStorageProv = _localStorageProv;
        this.editMode = false;
        this._history = [];
        this.newData = new ModelCreatorWidgetPlace();
    }
    ngOnInit() {
        this._reset();
        this.historyObserver$ = this.creator.getWidgetPlaceHistory(this.name).map(res => {
            this._history = res;
            return res;
        });
        this.widgetsSubscription$ = this.creator.getActiveWidgetPlace(this.name).subscribe((res) => {
            this._localStorageProv.remove(this.name);
            if (res) {
                this._localStorageProv.set(this.name, res);
                this.currentActiveContent = res;
                this.setData(res);
            }
            else {
                this._reset();
            }
        });
    }
    ngOnDestroy() {
        if (this.widgetsSubscription$)
            this.widgetsSubscription$.unsubscribe();
    }
    setData(data) {
        this.originalData = data;
        this.tmpOriginalData = this.helperProv.copy(this.originalData);
    }
    selectHistory(id) {
        const                  historyData = this._history.find(entry => entry.id === id);
        this.setData(historyData);
    }
    _reset() {
        let                  storage = this._localStorageProv.get(this.name);
        let                  tmp = storage ? new ModelCreatorWidgetPlace(storage) : new ModelCreatorWidgetPlace();
        this.setData(tmp);
    }
    update() {
        this.tmpOriginalData.data.active = new Date(this.tmpOriginalData.data.active);
        this.tmpOriginalData.data.widgets = this.helperProv.copy(this.tmpOriginalData.data.widgets);
        if (this.tmpOriginalData.id) {
            this.creator.update(this.name, this.tmpOriginalData.id, this.tmpOriginalData.data).then(res => {
                this.helperProv.toast('History updated');
                this.selectHistory(this.tmpOriginalData.id);
            });
        }
        else {
            this.creator.add(this.name, this.tmpOriginalData.data).then(res => {
                this.helperProv.toast('History added');
                this.selectHistory(res.id);
            });
        }
    }
    revertAll() {
        return this.tmpOriginalData = this.helperProv.copy(this.originalData);
    }
    compareFn(e1, e2) {
        return e1 && e2 ? e1.id === e2.id : e1 === e2;
    }
    deleteHistory() {
        const                  confirm = window.confirm('Are you sure you want to delete it?');
        if (confirm) {
            this.creator.delete(this.name, this.tmpOriginalData.id).then(res => {
                this._reset();
            });
        }
    }
}
CreatorComponent.decorators = [
    { type: Component, args: [{
                selector: 'creator',
                template: `<div [ngClass]="{'editMode' : creator.editMode}" *ngIf="tmpOriginalData">
  <ion-toolbar *ngIf="creator.editMode" color="dark">
    <ion-buttons left>
      <button ion-button small (click)="editMode = !editMode">✎</button>
    </ion-buttons>
    <ion-grid no-padding>
      <ion-row>
        <ion-col>
          <ion-item color="dark" no-lines>
            <ion-label>Template</ion-label>
            <ion-select [ngModel]="tmpOriginalData" [compareWith]="compareFn" #history (ionChange)="setData(history.value)">
              <ion-option [value]="newData">New template</ion-option>
              <ion-option *ngFor="let item of historyObserver$ | async" [value]="item">
                {{item.data.name || 'Template X'}}
                <span *ngIf="helperProv.compare(item, currentActiveContent)">◉</span>
              </ion-option>
            </ion-select>
          </ion-item>
        </ion-col>
      </ion-row>
    </ion-grid>
    <ion-buttons right>
      <button clear ion-button [disabled]="helperProv.compare(originalData, tmpOriginalData)" (click)="revertAll()">
        ↺
      </button>
      <button clear ion-button [disabled]="helperProv.compare(originalData, tmpOriginalData)" (click)="update()">
        ✔
      </button>
    </ion-buttons>
    <div *ngIf="editMode">
      <ion-item color="dark">
        <ion-label stacked class="required">Name</ion-label>
        <ion-input name="historyEntryName" type="text" [(ngModel)]="tmpOriginalData.data.name" required></ion-input>
      </ion-item>
      <ion-item color="dark">
        <ion-label stacked>Active from</ion-label>
        <ion-datetime [(ngModel)]="tmpOriginalData.data.active"
                      displayFormat="DD.MM.YYYY HH:mm UTC"
                      pickerFormat="DD.MM.YYYYTHH:mm"
                      doneText="{{ 'BUTTON.SAVE' | translate }}"
                      cancelText="{{ 'BUTTON.ABORT' | translate }}"></ion-datetime>
      </ion-item>
      <ion-toolbar>
        <ion-buttons>
          <button ion-button *ngIf="tmpOriginalData.id" (click)="deleteHistory()" color="danger" title="Delete">Delete</button>
        </ion-buttons>
      </ion-toolbar>
    </div>
  </ion-toolbar>
  <creator-widgets [(widgets)]="tmpOriginalData.data.widgets"></creator-widgets>
</div>`
            },] },
];
CreatorComponent.ctorParameters = () => [
    { type: CreatorProvider, },
    { type: HelperProvider, },
    { type: LocalstorageProvider, },
];
CreatorComponent.propDecorators = {
    "name": [{ type: Input },],
};

class CreatorTextDirective {
    constructor(creatorProv, _el) {
        this.creatorProv = creatorProv;
        this._el = _el;
        this._changeEditMode = (editMode) => {
            if (!this.disabled) {
                if (editMode) {
                    this._el.nativeElement.setAttribute('contenteditable', true);
                }
                else {
                    this._el.nativeElement.removeAttribute('contenteditable');
                }
            }
        };
        this.propagateChange = (_) => {
        };
        this.OnChange = new EventEmitter();
        this._disabled = false;
    }
    ngOnInit() {
        this._changeEditMode(this.creatorProv.editMode);
        this._onSwitchSubscription = this.creatorProv.OnSwitchEditMode.subscribe(this._changeEditMode);
    }
    ngOnDestroy() {
        this._onSwitchSubscription.unsubscribe();
    }
    registerOnChange(fn) {
        this.propagateChange = fn;
    }
    registerOnTouched() {
    }
    writeValue(value) {
        if (value !== undefined) {
            this.inputValue = value;
            this._el.nativeElement.innerText = this.inputValue;
        }
    }
    get disabled() {
        return (this._disabled === '') ? true : !!this._disabled;
    }
    get inputValue() {
        return this._inputValue;
    }
    set inputValue(val) {
        this._inputValue = (isNaN(val)) ? val : Number.parseInt(val);
    }
    onKeyup() {
        this.inputValue = this._el.nativeElement.innerText;
        this.propagateChange(this._inputValue);
    }
}
CreatorTextDirective.decorators = [
    { type: Directive, args: [{
                selector: '[creator-text]',
                host: {
                    '(input)': 'onKeyup()'
                },
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: forwardRef(() => CreatorTextDirective),
                    }
                ]
            },] },
];
CreatorTextDirective.ctorParameters = () => [
    { type: CreatorProvider, },
    { type: ElementRef, },
];
CreatorTextDirective.propDecorators = {
    "_inputValue": [{ type: Input },],
    "OnChange": [{ type: Output, args: ['change',] },],
    "_disabled": [{ type: Input, args: ['disabled',] },],
};

class KeysPipe {
    transform(value, args) {
        return Object.keys(value).map(key => {
            return { key: key, value: value[key] };
        });
    }
}
KeysPipe.decorators = [
    { type: Pipe, args: [{
                name: 'keys',
            },] },
];

class SafehtmlPipe {
    constructor(_sanitizer) {
        this._sanitizer = _sanitizer;
    }
    transform(value, ...args) {
        return this._sanitizer.bypassSecurityTrustHtml(value);
    }
}
SafehtmlPipe.decorators = [
    { type: Pipe, args: [{
                name: 'safehtml',
            },] },
];
SafehtmlPipe.ctorParameters = () => [
    { type: DomSanitizer, },
];

class TrustedurlPipe {
    constructor(_sanitizer) {
        this._sanitizer = _sanitizer;
    }
    transform(value, ...args) {
        return this._sanitizer.bypassSecurityTrustResourceUrl(value);
    }
}
TrustedurlPipe.decorators = [
    { type: Pipe, args: [{
                name: 'trustedurl',
            },] },
];
TrustedurlPipe.ctorParameters = () => [
    { type: DomSanitizer, },
];

class PipesModule {
}
PipesModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    KeysPipe,
                    SafehtmlPipe,
                    TrustedurlPipe,
                ],
                imports: [],
                exports: [
                    KeysPipe,
                    SafehtmlPipe,
                    TrustedurlPipe,
                ]
            },] },
];

class CreatorModule {
    static forRoot(config) {
        return {
            ngModule: CreatorModule,
            providers: [
                CreatorProvider,
                {
                    provide: CreatorWidgetsProvider,
                    useValue: new CreatorWidgetsProvider(config.customWidgets)
                }
            ]
        };
    }
}
CreatorModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    CkeditorComponent,
                    CreatorTextDirective,
                    CreatorComponent,
                    CreatorToolsComponent,
                    CreatorToolsLoginComponent,
                    CreatorToolsWidgetAddComponent,
                    CreatorWidgetComponent,
                    CreatorWidgetDemoComponent,
                    CreatorWidgetDemoSettingsComponent,
                    CreatorWidgetImageComponent,
                    CreatorWidgetImageSettingsComponent,
                    CreatorWidgetTableComponent,
                    CreatorWidgetTableSettingsComponent,
                    CreatorWidgetParagraphComponent,
                    CreatorWidgetHeadlineComponent,
                    CreatorWidgetHeadlineSettingsComponent,
                    CreatorWidgetsComponent,
                ],
                entryComponents: [
                    CreatorToolsLoginComponent,
                    CreatorToolsWidgetAddComponent,
                    CreatorWidgetDemoComponent,
                    CreatorWidgetDemoSettingsComponent,
                    CreatorWidgetImageComponent,
                    CreatorWidgetImageSettingsComponent,
                    CreatorWidgetTableComponent,
                    CreatorWidgetTableSettingsComponent,
                    CreatorWidgetParagraphComponent,
                    CreatorWidgetHeadlineComponent,
                    CreatorWidgetHeadlineSettingsComponent,
                ],
                providers: [
                    CreatorProvider,
                    HelperProvider,
                    LocalstorageProvider
                ],
                imports: [
                    PipesModule,
                    TranslateModule.forRoot(),
                    HttpClientModule,
                    AngularFireModule,
                    AngularFirestoreModule,
                    AngularFireAuthModule,
                    IonicModule,
                    CommonModule,
                    NgxDnDModule
                ],
                exports: [
                    CreatorComponent,
                    CreatorToolsComponent
                ]
            },] },
];

export { CreatorModule, CreatorProvider, ModelCreatorWidget, ModelCreatorWidgetHeadline, ModelCreatorWidgetParagraph, ModelCreatorWidgetImage, ModelCreatorWidgetDemo, ModelCreatorWidgetTable, ModelCreatorWidgetPlace, CkeditorComponent as ɵa, CreatorToolsComponent as ɵh, CreatorToolsLoginComponent as ɵg, CreatorToolsWidgetAddComponent as ɵf, CreatorWidgetDemoComponent as ɵk, CreatorWidgetDemoSettingsComponent as ɵl, CreatorWidgetHeadlineComponent as ɵr, CreatorWidgetHeadlineSettingsComponent as ɵs, CreatorWidgetImageComponent as ɵm, CreatorWidgetImageSettingsComponent as ɵn, CreatorWidgetParagraphComponent as ɵq, CreatorWidgetTableComponent as ɵo, CreatorWidgetTableSettingsComponent as ɵp, CreatorWidgetComponent as ɵj, CreatorWidgetsComponent as ɵt, CreatorComponent as ɵd, CreatorTextDirective as ɵc, KeysPipe as ɵv, PipesModule as ɵu, SafehtmlPipe as ɵw, TrustedurlPipe as ɵx, CreatorWidgetsProvider as ɵi, HelperProvider as ɵb, LocalstorageProvider as ɵe };
//# sourceMappingURL=ionic.creator.js.map
