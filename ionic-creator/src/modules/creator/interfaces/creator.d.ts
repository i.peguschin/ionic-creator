export declare module InterfaceCreator {
    interface Widget {
        type: string;
        [key: string]: any;
    }
}
