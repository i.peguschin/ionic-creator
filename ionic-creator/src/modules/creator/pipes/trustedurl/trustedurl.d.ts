import { PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SafeResourceUrl } from '@angular/platform-browser/src/security/dom_sanitization_service';
export declare class TrustedurlPipe implements PipeTransform {
    private _sanitizer;
    /**
     * Transforms a Value to SafeResourceUrl
     * @param _sanitizer
     */
    constructor(_sanitizer: DomSanitizer);
    /**
     *
     * @param value
     * @param args
     * @returns SafeResourceUrl
     */
    transform(value: string, ...args: any[]): SafeResourceUrl;
}
