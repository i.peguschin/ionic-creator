import { PipeTransform } from '@angular/core';
/**
 * Generated class for the KeysPipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
export declare class KeysPipe implements PipeTransform {
    /**
     * Takes a value and makes it lowercase.
     * @param value
     * @param args
     *
     */
    transform(value: any, args: string[]): {
        key: string;
        value: any;
    }[];
}
