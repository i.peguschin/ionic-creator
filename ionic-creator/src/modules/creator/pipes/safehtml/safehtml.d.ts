import { PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SafeHtml } from '@angular/platform-browser/src/security/dom_sanitization_service';
export declare class SafehtmlPipe implements PipeTransform {
    private _sanitizer;
    /**
     * Transforms a Value to SafeHtml
     * @param _sanitizer
     */
    constructor(_sanitizer: DomSanitizer);
    /**
     * Bypass security and trust the given value to be SafeHtml value
     * @param value
     * @param args
     *
     */
    transform(value: string, ...args: any[]): SafeHtml;
}
