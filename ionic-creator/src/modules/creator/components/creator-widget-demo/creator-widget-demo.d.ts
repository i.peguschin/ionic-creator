import { NavParams } from 'ionic-angular';
import { ModelCreatorWidgetDemo, ModelCreatorWidgetImage } from '../../models/CreatorWidgets';
import { CreatorProvider } from '../../providers/creator/creator';
export declare class CreatorWidgetDemoComponent {
    creatorProv: CreatorProvider;
    widget: ModelCreatorWidgetDemo;
    settingsComponent: typeof CreatorWidgetDemoSettingsComponent;
    constructor(creatorProv: CreatorProvider);
}
export declare class CreatorWidgetDemoSettingsComponent {
    private navParams;
    data: ModelCreatorWidgetImage;
    constructor(navParams: NavParams);
}
