import { ModelCreatorWidgetParagraph } from '../../models/CreatorWidgets';
import { CreatorProvider } from '../../providers/creator/creator';
export declare class CreatorWidgetParagraphComponent {
    creatorProv: CreatorProvider;
    widget: ModelCreatorWidgetParagraph;
    constructor(creatorProv: CreatorProvider);
}
