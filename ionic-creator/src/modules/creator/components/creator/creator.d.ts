import { OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ModelCreatorWidgetPlace } from '../../models/CreatorWidgets';
import { CreatorProvider } from '../../providers/creator/creator';
import { HelperProvider } from '../../providers/helper/helper';
import { LocalstorageProvider } from '../../providers/localstorage/localstorage';
export declare class CreatorComponent implements OnDestroy {
    creator: CreatorProvider;
    helperProv: HelperProvider;
    private _localStorageProv;
    name: string;
    editMode: boolean;
    widgetsSubscription$: any;
    _history: Array<ModelCreatorWidgetPlace>;
    historyObserver$: Observable<Array<ModelCreatorWidgetPlace>>;
    newData: ModelCreatorWidgetPlace;
    currentActiveContent: ModelCreatorWidgetPlace;
    originalData: ModelCreatorWidgetPlace;
    tmpOriginalData: ModelCreatorWidgetPlace;
    constructor(creator: CreatorProvider, helperProv: HelperProvider, _localStorageProv: LocalstorageProvider);
    ngOnInit(): void;
    ngOnDestroy(): void;
    /**
     * Set data
     * @param data
     */
    setData(data: ModelCreatorWidgetPlace): void;
    /**
     * Select histroy by id
     * @param id
     */
    selectHistory(id: any): void;
    /**
     * reset defaults
  
     */
    _reset(): void;
    /**
     * Update actions
     */
    update(): void;
    /**
     * Revert changes
     *
     */
    revertAll(): any;
    /**
     *
     * @param e1
     * @param e2
     *
     */
    compareFn(e1: any, e2: any): boolean;
    /**
     * Delete
     */
    deleteHistory(): void;
}
