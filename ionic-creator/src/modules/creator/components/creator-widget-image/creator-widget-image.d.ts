import { NavParams } from 'ionic-angular';
import { ModelCreatorWidgetImage } from '../../models/CreatorWidgets';
import { CreatorProvider } from '../../providers/creator/creator';
export declare class CreatorWidgetImageComponent {
    creator: CreatorProvider;
    widget: ModelCreatorWidgetImage;
    settingsComponent: typeof CreatorWidgetImageSettingsComponent;
    constructor(creator: CreatorProvider);
}
export declare class CreatorWidgetImageSettingsComponent {
    private navParams;
    data: ModelCreatorWidgetImage;
    constructor(navParams: NavParams);
}
