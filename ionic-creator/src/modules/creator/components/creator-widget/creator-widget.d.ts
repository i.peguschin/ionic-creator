import { ComponentFactoryResolver, EventEmitter, ViewContainerRef } from '@angular/core';
import { PopoverController } from 'ionic-angular';
import { InterfaceCreator } from '../../interfaces/creator';
import { CreatorWidgetsProvider } from '../../providers/creator/creator-widgets';
import { CreatorProvider } from '../../providers/creator/creator';
import { HelperProvider } from '../../providers/helper/helper';
export declare class CreatorWidgetComponent {
    helperProv: HelperProvider;
    creatorProv: CreatorProvider;
    private creatorWidgetsProv;
    private componentFactoryResolver;
    private _popoverCtrl;
    container: ViewContainerRef;
    data: InterfaceCreator.Widget;
    dataChange: EventEmitter<InterfaceCreator.Widget>;
    remove: EventEmitter<any>;
    moveDown: EventEmitter<any>;
    moveUp: EventEmitter<any>;
    componentRefInstance: any;
    dataBackup: InterfaceCreator.Widget;
    constructor(helperProv: HelperProvider, creatorProv: CreatorProvider, creatorWidgetsProv: CreatorWidgetsProvider, componentFactoryResolver: ComponentFactoryResolver, _popoverCtrl: PopoverController);
    _loadWidget(widget: any): boolean;
    ngOnInit(): void;
    revertAction(): void;
    openSettings(event: any): void;
}
