import { AngularFireAuth } from 'angularfire2/auth';
import { NavParams, PopoverController, ViewController } from 'ionic-angular';
import { CreatorProvider } from '../../providers/creator/creator';
import { CreatorWidgetsProvider } from '../../providers/creator/creator-widgets';
import { HelperProvider } from '../../providers/helper/helper';
export declare class CreatorToolsWidgetAddComponent {
    private _navParams;
    private viewCtrl;
    creatorWidgets: CreatorWidgetsProvider;
    constructor(_navParams: NavParams, viewCtrl: ViewController, creatorWidgets: CreatorWidgetsProvider);
    add(type: string): void;
}
export declare class CreatorToolsLoginComponent {
    private angularFireAuth;
    private _helper;
    creatorProv: CreatorProvider;
    user: string;
    pwd: string;
    constructor(angularFireAuth: AngularFireAuth, _helper: HelperProvider, creatorProv: CreatorProvider);
    login(): void;
}
export declare class CreatorToolsComponent {
    creator: CreatorProvider;
    private _popoverCtrl;
    constructor(creator: CreatorProvider, _popoverCtrl: PopoverController);
    presentPopover(e: any): void;
}
