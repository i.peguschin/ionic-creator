import { NavParams } from 'ionic-angular';
import { ModelCreatorWidgetHeadline } from '../../models/CreatorWidgets';
export declare class CreatorWidgetHeadlineComponent {
    widget: ModelCreatorWidgetHeadline;
    settingsComponent: typeof CreatorWidgetHeadlineSettingsComponent;
    constructor();
}
export declare class CreatorWidgetHeadlineSettingsComponent {
    private navParams;
    data: ModelCreatorWidgetHeadline;
    constructor(navParams: NavParams);
}
