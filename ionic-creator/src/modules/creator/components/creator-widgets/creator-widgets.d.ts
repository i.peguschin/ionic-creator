import { EventEmitter } from '@angular/core';
import { PopoverController } from 'ionic-angular';
import { Subject } from 'rxjs/Subject';
import { InterfaceCreator } from '../../interfaces/creator';
import { ModelCreatorWidget } from '../../models/CreatorWidgets';
import { CreatorProvider } from '../../providers/creator/creator';
import { HelperProvider } from '../../providers/helper/helper';
export declare class CreatorWidgetsComponent {
    private _helperProv;
    private _popoverCtrl;
    creatorProv: CreatorProvider;
    widgets: Array<ModelCreatorWidget>;
    widgetsChange: EventEmitter<Array<ModelCreatorWidget>>;
    actionLog: Subject<{
        action: string;
        widget: InterfaceCreator.Widget;
    }>;
    constructor(_helperProv: HelperProvider, _popoverCtrl: PopoverController, creatorProv: CreatorProvider);
    log(event: {
        type: string;
        value: any;
    }): void;
    _emit(): void;
    moveWidget(fromIndex: any, toIndex: any): void;
    removeWidget(index: any): void;
    addWidget(event: any): void;
    exportImport(): void;
}
