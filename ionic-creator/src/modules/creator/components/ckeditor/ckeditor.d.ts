import { ElementRef, EventEmitter } from '@angular/core';
import { HelperProvider } from '../../providers/helper/helper';
export declare class CkeditorComponent {
    private _helperProv;
    /**
     * Editor settings
     */
    private _useEditor;
    private _editors;
    private _editorConfig;
    editorInstance: any;
    editor: ElementRef;
    text: string;
    textChange: EventEmitter<string>;
    constructor(_helperProv: HelperProvider);
    /**
     *
     *
  
     */
    _loadScript(): Promise<{}>;
    /**
     * change event
  
     */
    _bindChanges: () => void;
    ngOnInit(): void;
    ngOnDestroy(): void;
}
