import { NavParams } from 'ionic-angular';
import { ModelCreatorWidgetTable } from '../../models/CreatorWidgets';
import { CreatorProvider } from '../../providers/creator/creator';
export declare class CreatorWidgetTableComponent {
    creatorProv: CreatorProvider;
    widget: ModelCreatorWidgetTable;
    settingsComponent: typeof CreatorWidgetTableSettingsComponent;
    constructor(creatorProv: CreatorProvider);
    move(from: any, to: any): this;
    removeCol(index: any): void;
    moveLeft(index: any): void;
    moveRight(index: any): void;
}
export declare class CreatorWidgetTableSettingsComponent {
    private navParams;
    data: ModelCreatorWidgetTable;
    constructor(navParams: NavParams);
    addCol(): void;
}
