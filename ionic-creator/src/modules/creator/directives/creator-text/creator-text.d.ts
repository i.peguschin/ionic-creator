import { ElementRef, EventEmitter } from '@angular/core';
import { CreatorProvider } from '../../providers/creator/creator';
export declare class CreatorTextDirective {
    creatorProv: CreatorProvider;
    private _el;
    private _onSwitchSubscription;
    constructor(creatorProv: CreatorProvider, _el: ElementRef);
    _changeEditMode: (editMode: any) => void;
    ngOnInit(): void;
    ngOnDestroy(): void;
    /**
     * Standard Accessor methods
     */
    propagateChange: (_: any) => void;
    /**
     * Standard ngModel register function
     * @param fn
     */
    registerOnChange(fn: any): void;
    /**
     * Standard ngModel touched function
     */
    registerOnTouched(): void;
    /**
     * Standard ngModel write function
     */
    writeValue(value: any): void;
    /**
     * value input (ngModel)
     *
  
     */
    _inputValue: any;
    /**
     * Change Output
     *
     */
    OnChange: EventEmitter<HTMLSelectElement>;
    /**
     * Disabled input
     *
  
     */
    private _disabled;
    /**
     * getter is disabled
     *
     */
    readonly disabled: boolean;
    /**
     * Getter input value
     *
     */
    /**
     * Setter input value
     * @param val
     */
    inputValue: any;
    /**
     * manual change trigger
     */
    onKeyup(): void;
}
