import { CreatorProvider } from './providers/creator/creator';
import { CreatorWidgetsProvider } from './providers/creator/creator-widgets';
export declare class CreatorModule {
    static forRoot(config: {
        customWidgets: {
            [key: string]: {
                component: any;
                model: any;
            };
        };
    }): {
        ngModule: typeof CreatorModule;
        providers: (typeof CreatorProvider | {
            provide: typeof CreatorWidgetsProvider;
            useValue: CreatorWidgetsProvider;
        })[];
    };
}
