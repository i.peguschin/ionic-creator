import { InterfaceCreator } from '../interfaces/creator';
export declare class ModelCreatorWidget {
    type: string;
    constructor(widget: any);
}
export declare class ModelCreatorWidgetHeadline extends ModelCreatorWidget {
    text: string;
    headline: number;
    constructor(widget: any);
}
export declare class ModelCreatorWidgetParagraph extends ModelCreatorWidget {
    text: string;
    constructor(widget: any);
}
export declare class ModelCreatorWidgetImage extends ModelCreatorWidget {
    src: string;
    link: string;
    linkType: string;
    constructor(widget: any);
}
export declare class ModelCreatorWidgetDemo extends ModelCreatorWidget {
    foo: string;
    bar: string;
    constructor(widget: any);
}
export declare class ModelCreatorWidgetTable extends ModelCreatorWidget {
    cols: Array<{
        widgets: Array<ModelCreatorWidget>;
    }>;
    constructor(widget: InterfaceCreator.Widget);
}
export declare class ModelCreatorWidgetPlace {
    id: string;
    data: {
        active: Date;
        name: string;
        widgets: Array<InterfaceCreator.Widget>;
    };
    constructor(place?: {
        id: string;
        data: any;
    });
}
