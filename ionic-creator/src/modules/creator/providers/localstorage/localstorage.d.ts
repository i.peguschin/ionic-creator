import 'rxjs/add/operator/map';
/**
 * Local storage provider
 */
export declare class LocalstorageProvider {
    /**
     * Local storage data object
     */
    data: any;
    /**
     * Get local storage item
     * @param key
     * @param toJSON
     *
     */
    get(key: any, toJSON?: boolean): any;
    /**
     * Set local storage item
     * @param key
     * @param data
     * @param toString
     */
    set(key: any, data: any, toString?: boolean): void;
    /**
     * Remove local storage item
     * @param key
     */
    remove(key: any): void;
}
