import 'rxjs/add/operator/map';
import { CreatorWidgetDemoComponent } from '../../components/creator-widget-demo/creator-widget-demo';
import { CreatorWidgetHeadlineComponent } from '../../components/creator-widget-headline/creator-widget-headline';
import { CreatorWidgetImageComponent } from '../../components/creator-widget-image/creator-widget-image';
import { CreatorWidgetParagraphComponent } from '../../components/creator-widget-paragraph/creator-widget-paragraph';
import { CreatorWidgetTableComponent } from '../../components/creator-widget-table/creator-widget-table';
import { InterfaceCreator } from '../../interfaces/creator';
import { ModelCreatorWidgetDemo, ModelCreatorWidgetHeadline, ModelCreatorWidgetImage, ModelCreatorWidgetParagraph, ModelCreatorWidgetTable } from '../../models/CreatorWidgets';
export declare class CreatorWidgetsProvider {
    widgets: {
        demo: {
            component: typeof CreatorWidgetDemoComponent;
            model: typeof ModelCreatorWidgetDemo;
        };
        headline: {
            component: typeof CreatorWidgetHeadlineComponent;
            model: typeof ModelCreatorWidgetHeadline;
        };
        paragraph: {
            component: typeof CreatorWidgetParagraphComponent;
            model: typeof ModelCreatorWidgetParagraph;
        };
        image: {
            component: typeof CreatorWidgetImageComponent;
            model: typeof ModelCreatorWidgetImage;
        };
        table: {
            component: typeof CreatorWidgetTableComponent;
            model: typeof ModelCreatorWidgetTable;
        };
    };
    modelize(widgets: Array<InterfaceCreator.Widget>): any[];
    constructor(configCustomWidgets: any);
}
