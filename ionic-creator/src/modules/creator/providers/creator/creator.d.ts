import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import { Events } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { ModelCreatorWidgetPlace } from '../../models/CreatorWidgets';
import { HelperProvider } from '../helper/helper';
import DocumentReference = firebase.firestore.DocumentReference;
export declare class CreatorProvider {
    private _afs;
    private _afa;
    private _helperProv;
    private _events;
    user: firebase.User;
    show: boolean;
    editMode: boolean;
    OnSwitchEditMode: Observable<{}>;
    constructor(_afs: AngularFirestore, _afa: AngularFireAuth, _helperProv: HelperProvider, _events: Events);
    /**
     *
     * @param res
  
     */
    private _mapSnapshotChanges(res);
    /**
     *
     * @param widgetPlaceId
     *
     */
    getWidgetPlaceHistory(widgetPlaceId: any): Observable<Array<ModelCreatorWidgetPlace>>;
    getActiveWidgetPlace(widgetPlaceId: any): Observable<ModelCreatorWidgetPlace>;
    update(widgetPlaceId: any, historyId: any, data?: {}): Promise<void>;
    add(widgetPlaceId: any, data: any): Promise<DocumentReference>;
    delete(widgetPlaceId: any, historyId: any): Promise<void>;
}
