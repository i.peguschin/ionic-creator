import { NgZone } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Alert, AlertController, App, Content, ToastController, ViewController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
/**
 * Helper provider for various functions
 */
export declare class HelperProvider {
    private _toastCtrl;
    private _translate;
    private _alertCtrl;
    private _app;
    private _zone;
    /**
     * Active component content
     */
    content: Content;
    /**
     *
     * type {Observable<any>}
     */
    OnResize: Observable<{}>;
    /**
     *
     * @param _toastCtrl
     * @param _translate
     * @param _alertCtrl
     * @param _app
     * @param _zone
     */
    constructor(_toastCtrl: ToastController, _translate: TranslateService, _alertCtrl: AlertController, _app: App, _zone: NgZone);
    /**
     *
     * @param message
     * @param position
     * @param translate
     */
    toast(message: any, position?: string, translate?: boolean): void;
    /**
     *
     * @description Replace special caracters
     * @param s
     *  String mit ersetzten Zeichen
     */
    replaceUmlauts(s: any): any;
    /**
     *
     * @description Überprüft ob die Anwendung unter "Locahost" läuft
     *
     */
    readonly isDev: boolean;
    /**
     * Überprüft ob die Variable existiert
     *
     */
    readonly isCrawler: any;
    /**
     * Individual alert
     * @param title
     * @param message
     * @param translate
     * @param interpolateParams
     *
     */
    alert(title: any, message: any, translate?: boolean, interpolateParams?: Object): Alert;
    /**
     * Confirm
     * @param title
     * @param message
     * @param translate
     *
     */
    confirm(title: any, message: any, translate?: boolean): Promise<{}>;
    /**
     * @description Gibt den aktuellen Unix timestamp zurück
     *  timestamp Timestamp
     */
    readonly timestamp: number;
    /**
     * Find index object in array of objects
     * @param objectNeedle
     * @param ArrayStack
     *
     */
    findIndexOfObjectInArray(objectNeedle: any, ArrayStack: Array<any>): number;
    /**
     * Compare objects
     * @param a
     * @param b
     *
     */
    compare(a: any, b: any): boolean;
    /**
     * Generate copy of object without object references
     * @param a
     * @param classObject
     *
     */
    copy(a: any, classObject?: any): any;
    /**
     * Round number
     * .round(5.2123123, 10) => 5.2
     * .round(5.2123123, 100) => 5.21
     * @param x
     * @param n
     *
     */
    round(x: any, n: any): number;
    /**
     * Load src manually
     * @param filePath
     * @param filetype
     *
     */
    loadSrc(filePath: string, filetype: string): Promise<{}>;
    /**
     * Get current view controller
     *
     */
    getCurrentViewController(): ViewController;
    /**
     * Get current component instance
     *
     */
    getCurrentComponentInstance(): any;
    /**
     * Get component instance by page
     * @param page
     *
     */
    getComponentInstance(page: ViewController): any;
    /**
     * Generate random number
     *
     */
    randNum(min?: number, max?: number): number;
    /**
     * Promise stacker
     * Helper method do prevent doubled promises
     * usage: helperService.getPromise('loadData', (resolve, reject) => {resolve()|reject()})
     * @param key
     * @param func
     *
     */
    private _promises;
    private _promisesResolved;
    getPromise(key: any, func: any): Promise<any>;
}
