(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@ngx-translate/core'), require('ionic-angular'), require('rxjs/add/operator/map'), require('rxjs/Observable'), require('angularfire2/auth'), require('angularfire2/firestore'), require('rxjs/Subject'), require('@angular/forms'), require('@angular/platform-browser'), require('@angular/common'), require('@angular/common/http'), require('@swimlane/ngx-dnd'), require('angularfire2')) :
	typeof define === 'function' && define.amd ? define('ionic.creator', ['exports', '@angular/core', '@ngx-translate/core', 'ionic-angular', 'rxjs/add/operator/map', 'rxjs/Observable', 'angularfire2/auth', 'angularfire2/firestore', 'rxjs/Subject', '@angular/forms', '@angular/platform-browser', '@angular/common', '@angular/common/http', '@swimlane/ngx-dnd', 'angularfire2'], factory) :
	(factory((global.ionic = global.ionic || {}, global.ionic.creator = {}),global.ng.core,global.core$1,global.ionicAngular,global.Rx.Observable.prototype,global.Rx,global.auth,global.firestore,global.Rx,global.ng.forms,global.ng.platformBrowser,global.ng.common,global.ng.common.http,global.ngxDnd,global.angularfire2));
}(this, (function (exports,core,core$1,ionicAngular,map,Observable,auth,firestore,Subject,forms,platformBrowser,common,http,ngxDnd,angularfire2) { 'use strict';

var extendStatics = Object.setPrototypeOf ||
    ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
    function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}









function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}
function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

var HelperProvider =               (function () {
    function HelperProvider(_toastCtrl, _translate, _alertCtrl, _app, _zone) {
        var _this = this;
        this._toastCtrl = _toastCtrl;
        this._translate = _translate;
        this._alertCtrl = _alertCtrl;
        this._app = _app;
        this._zone = _zone;
        this.OnResize = new Observable.Observable(function (observer) {
            var timeout = null;
            window.addEventListener('resize', function (event) {
                clearTimeout(timeout);
                _this._zone.runOutsideAngular(function () {
                    timeout = setTimeout(function () {
                        observer.next(event);
                    }, 100);
                });
            });
            observer.next(event);
        });
        this._promises = {};
        this._promisesResolved = {};
    }
    HelperProvider.prototype.toast = function (message, position, translate) {
        if (position === void 0) { position = 'bottom'; }
        if (translate === void 0) { translate = true; }
        var toast = this._toastCtrl.create({
            message: (translate) ? this._translate.instant(message) : message,
            position: position,
            cssClass: 'text-center'
        });
        toast.present({ updateUrl: false });
        this._zone.runOutsideAngular(function () { return setTimeout(function () { return toast.dismiss(null, null, { updateUrl: false }); }, 3000); });
    };
    HelperProvider.prototype.replaceUmlauts = function (s) {
        return s.replace(/[äöüÄÖÜßáàâéèêíìîóòôúùûç]/g, function ($0) {
            var tr = {
                'ä': 'ae',
                'ü': 'ue',
                'ö': 'oe',
                'ß': 'ss',
                'Ä': 'AE',
                'Ü': 'UE',
                'Ö': 'OE',
                'á': 'a',
                'à': 'a',
                'â': 'a',
                'é': 'e',
                'è': 'e',
                'ê': 'e',
                'í': 'i',
                'ì': 'i',
                'î': 'i',
                'ó': 'o',
                'ò': 'o',
                'ô': 'o',
                'ú': 'u',
                'ù': 'u',
                'û': 'u',
                'ç': 'c'
            };
            return tr[$0];
        });
    };
    Object.defineProperty(HelperProvider.prototype, "isDev", {
        get: function () {
            return window.location.href.indexOf('localhost:') > -1;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HelperProvider.prototype, "isCrawler", {
        get: function () {
            var botPattern = "(bot|google|baidu|bing|msn|duckduckgo|teoma|slurp|yandex)";
            var re = new RegExp(botPattern, 'i');
            return window['isCrawler'] || re.test(navigator.userAgent) || false;
        },
        enumerable: true,
        configurable: true
    });
    HelperProvider.prototype.alert = function (title, message, translate, interpolateParams) {
        if (translate === void 0) { translate = true; }
        var alert = this._alertCtrl.create({
            title: (translate) ? this._translate.instant(title, interpolateParams) : title,
            subTitle: (translate) ? this._translate.instant(message, interpolateParams) : message,
            buttons: ['OK']
        });
        alert.present({ updateUrl: false });
        return alert;
    };
    HelperProvider.prototype.confirm = function (title, message, translate) {
        var _this = this;
        if (translate === void 0) { translate = true; }
        return new Promise(function (resolve, reject) {
            var confirm = _this._alertCtrl.create({
                title: (translate) ? _this._translate.instant(title) : title,
                message: (translate) ? _this._translate.instant(message) : message,
                buttons: [
                    {
                        text: _this._translate.instant('GLOBAL.NO'),
                        handler: reject
                    }, {
                        text: _this._translate.instant('GLOBAL.YES'),
                        handler: resolve
                    }
                ]
            });
            confirm.present();
        });
    };
    Object.defineProperty(HelperProvider.prototype, "timestamp", {
        get: function () {
            return Math.round(new Date().getTime() / 1000);
        },
        enumerable: true,
        configurable: true
    });
    HelperProvider.prototype.findIndexOfObjectInArray = function (objectNeedle, ArrayStack) {
        if (!Array.isArray(ArrayStack)) {
            ArrayStack = [];
        }
        return ArrayStack.findIndex(function (arrayElement) {
            return JSON.stringify(arrayElement) === JSON.stringify(objectNeedle);
        });
    };
    HelperProvider.prototype.compare = function (a, b) {
        return JSON.stringify(a) === JSON.stringify(b);
    };
    HelperProvider.prototype.copy = function (a, classObject) {
        var data = JSON.parse(JSON.stringify(a));
        if (classObject) {
            return Object.assign(new (classObject.bind.apply(classObject, __spread([void 0], data)))(), a);
        }
        else {
            return data;
        }
    };
    HelperProvider.prototype.round = function (x, n) {
        return (Math.round(x * n) / n);
    };
    HelperProvider.prototype.loadSrc = function (filePath, filetype) {
        return new Promise(function (resolve) {
            var fileref;
            if (filetype == "js") {
                fileref = document.createElement('script');
                fileref.onload = resolve;
                fileref.setAttribute("type", "text/javascript");
                fileref.setAttribute("src", filePath);
            }
            else if (filetype == "css") {
                fileref = document.createElement("link");
                fileref.setAttribute("rel", "stylesheet");
                fileref.onload = resolve;
                fileref.setAttribute("type", "text/css");
                fileref.setAttribute("href", filePath);
            }
            if (typeof fileref != "undefined") {
                document.getElementsByTagName("head")[0].appendChild(fileref);
            }
        });
    };
    HelperProvider.prototype.getCurrentViewController = function () {
        var nav = this._app.getActiveNavs()[0];
        return nav.getActive();
    };
    HelperProvider.prototype.getCurrentComponentInstance = function () {
        return this.getComponentInstance(this.getCurrentViewController());
    };
    HelperProvider.prototype.getComponentInstance = function (page) {
        return page.instance;
    };
    HelperProvider.prototype.randNum = function (min, max) {
        if (min === void 0) { min = 111111; }
        if (max === void 0) { max = 999999; }
        return Math.floor(Math.random() * (max - min + 1) + min);
    };
    HelperProvider.prototype.getPromise = function (key, func) {
        var _this = this;
        if (this._promises[key] && !this._promisesResolved[key]) {
            return this._promises[key];
        }
        this._promises[key] = new Promise(func);
        this._promisesResolved[key] = false;
        this._promises[key].then(function () {
            _this._promisesResolved[key] = true;
        }).catch(function () {
            _this._promisesResolved[key] = true;
        });
        return this._promises[key];
    };
    return HelperProvider;
}());
HelperProvider.decorators = [
    { type: core.Injectable },
];
HelperProvider.ctorParameters = function () { return [
    { type: ionicAngular.ToastController, },
    { type: core$1.TranslateService, },
    { type: ionicAngular.AlertController, },
    { type: ionicAngular.App, },
    { type: core.NgZone, },
]; };
HelperProvider.propDecorators = {
    "content": [{ type: core.ViewChild, args: [ionicAngular.Content,] },],
};
var CkeditorComponent =               (function () {
    function CkeditorComponent(_helperProv) {
        var _this = this;
        this._helperProv = _helperProv;
        this._useEditor = 'balloon';
        this._editors = {
            'balloon': 'BalloonEditor',
            'inline': 'InlineEditor',
            'classic': 'ClassicEditor'
        };
        this._editorConfig = {};
        this.textChange = new core.EventEmitter();
        this._bindChanges = function () {
            _this.textChange.emit(_this.editorInstance.getData());
        };
    }
    CkeditorComponent.prototype._loadScript = function () {
        var _this = this;
        return new Promise(function (resolve) {
            if (window[_this._editors[_this._useEditor]]) {
                resolve();
            }
            else {
                resolve(_this._helperProv.getPromise('load-ck-editor', function (resolve, reject) {
                    console.log('Load ckeditor..');
                    _this._helperProv.loadSrc('https://cdn.ckeditor.com/ckeditor5/1.0.0-beta.1/' +
                        _this._useEditor + '/ckeditor.js', 'js')
                        .then(resolve).catch(reject);
                }));
            }
        });
    };
    CkeditorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._loadScript().then(function () {
            window[_this._editors[_this._useEditor]].create(_this.editor.nativeElement, _this._editorConfig).then(function (editor) {
                _this.editorInstance = editor;
                _this.editorInstance.model.document.on('change', _this._bindChanges);
            }).catch(function (error) {
                console.error(error);
            });
        });
    };
    CkeditorComponent.prototype.ngOnDestroy = function () {
        if (this.editorInstance) {
            this.editorInstance.model.document.off('change', this._bindChanges);
            this.editorInstance.destroy();
        }
    };
    return CkeditorComponent;
}());
CkeditorComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ckeditor',
                template: "<div #editor [innerHtml]=\"text\"></div>"
            },] },
];
CkeditorComponent.ctorParameters = function () { return [
    { type: HelperProvider, },
]; };
CkeditorComponent.propDecorators = {
    "editor": [{ type: core.ViewChild, args: ['editor',] },],
    "text": [{ type: core.Input },],
    "textChange": [{ type: core.Output },],
};
var ModelCreatorWidget =               (function () {
    function ModelCreatorWidget(widget) {
        this.type = widget.type;
    }
    return ModelCreatorWidget;
}());
var ModelCreatorWidgetHeadline =               (function (_super) {
    __extends(ModelCreatorWidgetHeadline, _super);
    function ModelCreatorWidgetHeadline(widget) {
        var _this = _super.call(this, widget) || this;
        _this.text = widget.text || 'This is a headline!';
        _this.headline = widget.headline || 1;
        return _this;
    }
    return ModelCreatorWidgetHeadline;
}(ModelCreatorWidget));
var ModelCreatorWidgetParagraph =               (function (_super) {
    __extends(ModelCreatorWidgetParagraph, _super);
    function ModelCreatorWidgetParagraph(widget) {
        var _this = _super.call(this, widget) || this;
        _this.text = widget.text || '<p>Do you see any Teletubbies in here? Do you see a slender plastic tag clipped to my shirt with my name printed on it? Do you see a little Asian child with a blank expression on his face sitting outside on a mechanical helicopter that shakes when you put quarters in it? No? Well, that\'s what you see at a toy store. And you must think you\'re in a toy store, because you\'re here shopping for an infant named Jeb.</p>';
        return _this;
    }
    return ModelCreatorWidgetParagraph;
}(ModelCreatorWidget));
var ModelCreatorWidgetImage =               (function (_super) {
    __extends(ModelCreatorWidgetImage, _super);
    function ModelCreatorWidgetImage(widget) {
        var _this = _super.call(this, widget) || this;
        _this.src = widget.src || 'http://via.placeholder.com/350x150';
        _this.link = widget.link;
        _this.linkType = widget.linkType;
        return _this;
    }
    return ModelCreatorWidgetImage;
}(ModelCreatorWidget));
var ModelCreatorWidgetDemo =               (function (_super) {
    __extends(ModelCreatorWidgetDemo, _super);
    function ModelCreatorWidgetDemo(widget) {
        var _this = _super.call(this, widget) || this;
        _this.foo = widget.foo;
        _this.bar = widget.bar || 1;
        return _this;
    }
    return ModelCreatorWidgetDemo;
}(ModelCreatorWidget));
var ModelCreatorWidgetTable =               (function (_super) {
    __extends(ModelCreatorWidgetTable, _super);
    function ModelCreatorWidgetTable(widget) {
        var _this = _super.call(this, widget) || this;
        _this.cols = widget["cols"] || [{ widgets: [] }, { widgets: [] }];
        _this.cols = _this.cols.map(function (col) {
            col.widgets = col.widgets || [];
            return col;
        });
        return _this;
    }
    return ModelCreatorWidgetTable;
}(ModelCreatorWidget));
var ModelCreatorWidgetPlace =               (function () {
    function ModelCreatorWidgetPlace(place) {
        if (place === void 0) { place = {
            id: null,
            data: {}
        }; }
        this.id = place.id;
        this.data = place.data;
        this.data.active = new Date(this.data.active);
        this.data.name = this.data.name || '';
        this.data.widgets = this.data.widgets || [];
    }
    return ModelCreatorWidgetPlace;
}());
var CreatorProvider =               (function () {
    function CreatorProvider(_afs, _afa, _helperProv, _events) {
        var _this = this;
        this._afs = _afs;
        this._afa = _afa;
        this._helperProv = _helperProv;
        this._events = _events;
        this.show = false;
        this.editMode = false;
        this.OnSwitchEditMode = new Observable.Observable(function (observe) {
            _this._events.subscribe('OnSwitchEditMode', function (mode) { return observe.next(mode); });
        });
        this._afa.authState.subscribe(function (user) {
            _this.user = user;
            if (_this.user) {
                _this.editMode = true;
            }
        });
        var holdKeys = '';
        var activationKey = 17;
        var timeout = null;
        document.addEventListener("keydown", function (e) {
            var keyCode = e.keyCode;
            if (keyCode == activationKey) {
                if (holdKeys == ('' + activationKey + activationKey + activationKey)) {
                    _this.show = true;
                    if (_this.user) {
                        _this.editMode = !_this.editMode;
                        _this._events.publish('OnSwitchEditMode', _this.editMode);
                    }
                    clearTimeout(timeout);
                    timeout = setTimeout(function () {
                        holdKeys = '';
                        console.log('Reset holdKeys');
                    }, 3000);
                }
                else {
                    holdKeys += keyCode;
                }
            }
        }, false);
    }
    CreatorProvider.prototype._mapSnapshotChanges = function (res) {
        return res.map(function (a) {
            var data = a.payload.doc.data();
            var id = a.payload.doc.id;
            return {
                id: id,
                data: data
            };
        });
    };
    CreatorProvider.prototype.getWidgetPlaceHistory = function (widgetPlaceId) {
        var _this = this;
        return this._afs.doc('creator/widgets')
            .collection(widgetPlaceId, function (ref) {
            return ref.orderBy('active', 'desc');
        })
            .snapshotChanges()
            .map(function (res) { return _this._mapSnapshotChanges(res); })
            .map(function (res) {
            return res.map(function (data) {
                return new ModelCreatorWidgetPlace(data);
            });
        });
    };
    CreatorProvider.prototype.getActiveWidgetPlace = function (widgetPlaceId) {
        var _this = this;
        return this._afs.doc('creator/widgets')
            .collection(widgetPlaceId, function (ref) {
            return ref
                .where('active', '<=', new Date())
                .orderBy('active', 'desc')
                .limit(1);
        })
            .snapshotChanges()
            .map(function (res) { return _this._mapSnapshotChanges(res); })
            .map(function (res) {
            return res.map(function (data) {
                return new ModelCreatorWidgetPlace(data);
            })[0];
        });
    };
    CreatorProvider.prototype.update = function (widgetPlaceId, historyId, data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        var x = this._afs.doc('creator/widgets/' + widgetPlaceId + '/' + historyId).update(data);
        x.then(function () {
            _this._helperProv.toast('Wurde gespeichert!');
        });
        return x;
    };
    CreatorProvider.prototype.add = function (widgetPlaceId, data) {
        return this._afs.doc('creator/widgets/').collection(widgetPlaceId).add(data);
    };
    CreatorProvider.prototype.delete = function (widgetPlaceId, historyId) {
        return this._afs.doc('creator/widgets/' + widgetPlaceId + '/' + historyId).delete();
    };
    return CreatorProvider;
}());
CreatorProvider.decorators = [
    { type: core.Injectable },
];
CreatorProvider.ctorParameters = function () { return [
    { type: firestore.AngularFirestore, },
    { type: auth.AngularFireAuth, },
    { type: HelperProvider, },
    { type: ionicAngular.Events, },
]; };
var CreatorWidgetDemoComponent =               (function () {
    function CreatorWidgetDemoComponent(creatorProv) {
        this.creatorProv = creatorProv;
        this.settingsComponent = CreatorWidgetDemoSettingsComponent;
    }
    return CreatorWidgetDemoComponent;
}());
CreatorWidgetDemoComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'creator-widget-demo',
                template: "<pre>\nEditable: {{creatorProv.editMode}}\nWidget:  {{ widget | json }}\n</pre>\n"
            },] },
];
CreatorWidgetDemoComponent.ctorParameters = function () { return [
    { type: CreatorProvider, },
]; };
var CreatorWidgetDemoSettingsComponent =               (function () {
    function CreatorWidgetDemoSettingsComponent(navParams) {
        this.navParams = navParams;
        this.data = this.navParams.get('widget');
    }
    return CreatorWidgetDemoSettingsComponent;
}());
CreatorWidgetDemoSettingsComponent.decorators = [
    { type: core.Component, args: [{
                template: "<ion-list>\n  <ion-item>\n    <ion-label stacked>DEMO #1</ion-label>\n    <ion-input type=\"text\" name=\"foo\" [(ngModel)]=\"data.foo\"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label>Demo #2</ion-label>\n    <select name=\"bar\" [(ngModel)]=\"data.bar\">\n      <option value=\"1\">Setting 1</option>\n      <option value=\"2\">Setting 2</option>\n    </select>\n  </ion-item>\n</ion-list>\n"
            },] },
];
CreatorWidgetDemoSettingsComponent.ctorParameters = function () { return [
    { type: ionicAngular.NavParams, },
]; };
var CreatorWidgetHeadlineComponent =               (function () {
    function CreatorWidgetHeadlineComponent() {
        this.settingsComponent = CreatorWidgetHeadlineSettingsComponent;
    }
    return CreatorWidgetHeadlineComponent;
}());
CreatorWidgetHeadlineComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'creator-widget-headline',
                template: "<h1 *ngIf=\"widget.headline === 1\" creator-text [(ngModel)]=\"widget.text\"></h1>\n<h2 *ngIf=\"widget.headline === 2\" creator-text [(ngModel)]=\"widget.text\"></h2>\n<h3 *ngIf=\"widget.headline === 3\" creator-text [(ngModel)]=\"widget.text\"></h3>\n<h4 *ngIf=\"widget.headline === 4\" creator-text [(ngModel)]=\"widget.text\"></h4>\n<h5 *ngIf=\"widget.headline === 5\" creator-text [(ngModel)]=\"widget.text\"></h5>\n<h6 *ngIf=\"widget.headline === 6\" creator-text [(ngModel)]=\"widget.text\"></h6>\n"
            },] },
];
CreatorWidgetHeadlineComponent.ctorParameters = function () { return []; };
var CreatorWidgetHeadlineSettingsComponent =               (function () {
    function CreatorWidgetHeadlineSettingsComponent(navParams) {
        this.navParams = navParams;
        this.data = this.navParams.get('widget');
    }
    return CreatorWidgetHeadlineSettingsComponent;
}());
CreatorWidgetHeadlineSettingsComponent.decorators = [
    { type: core.Component, args: [{
                template: "<ion-list>\n  <ion-label>Headline size</ion-label>\n  <select name=\"headline\" [(ngModel)]=\"data.headline\">\n    <option [value]=\"h\" *ngFor=\"let h of [1,2,3,4,5,6]\">H{{h}}</option>\n  </select>\n</ion-list>\n"
            },] },
];
CreatorWidgetHeadlineSettingsComponent.ctorParameters = function () { return [
    { type: ionicAngular.NavParams, },
]; };
var CreatorWidgetImageComponent =               (function () {
    function CreatorWidgetImageComponent(creator) {
        this.creator = creator;
        this.settingsComponent = CreatorWidgetImageSettingsComponent;
    }
    return CreatorWidgetImageComponent;
}());
CreatorWidgetImageComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'creator-widget-image',
                template: "<div *ngIf=\"widget.link\" [ngSwitch]=\"widget.linkType\">\n  <a *ngSwitchCase=\"'extern'\" [href]=\"widget.link\" target=\"_blank\">\n    <img [src]=\"widget.src\">\n  </a>\n  <a *ngSwitchDefault [href]=\"widget.link\">\n    <img [src]=\"widget.src\">\n  </a>\n</div>\n<img *ngIf=\"!widget.link\" [src]=\"widget.src\">\n"
            },] },
];
CreatorWidgetImageComponent.ctorParameters = function () { return [
    { type: CreatorProvider, },
]; };
var CreatorWidgetImageSettingsComponent =               (function () {
    function CreatorWidgetImageSettingsComponent(navParams) {
        this.navParams = navParams;
        this.data = this.navParams.get('widget');
    }
    return CreatorWidgetImageSettingsComponent;
}());
CreatorWidgetImageSettingsComponent.decorators = [
    { type: core.Component, args: [{
                template: "<ion-list>\n  <ion-item>\n    <ion-label stacked>Bild Quelle</ion-label>\n    <ion-input type=\"text\" name=\"src\" [(ngModel)]=\"data.src\"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label stacked>Link</ion-label>\n    <ion-input type=\"text\" name=\"link\" [(ngModel)]=\"data.link\"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label>Link Typ</ion-label>\n    <select name=\"linkType\" [(ngModel)]=\"data.linkType\">\n      <option value=\"intern\" [selected]=\"!data.linkType\">intern</option>\n      <option value=\"extern\">extern</option>\n    </select>\n  </ion-item>\n</ion-list>\n"
            },] },
];
CreatorWidgetImageSettingsComponent.ctorParameters = function () { return [
    { type: ionicAngular.NavParams, },
]; };
var CreatorWidgetParagraphComponent =               (function () {
    function CreatorWidgetParagraphComponent(creatorProv) {
        this.creatorProv = creatorProv;
    }
    return CreatorWidgetParagraphComponent;
}());
CreatorWidgetParagraphComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'creator-widget-paragraph',
                template: "<div *ngIf=\"!creatorProv.editMode\" [innerHtml]=\"widget.text\"></div>\n<ckeditor *ngIf=\"creatorProv.editMode\" [(text)]=\"widget.text\"></ckeditor>\n"
            },] },
];
CreatorWidgetParagraphComponent.ctorParameters = function () { return [
    { type: CreatorProvider, },
]; };
var CreatorWidgetTableComponent =               (function () {
    function CreatorWidgetTableComponent(creatorProv) {
        this.creatorProv = creatorProv;
        this.settingsComponent = CreatorWidgetTableSettingsComponent;
    }
    CreatorWidgetTableComponent.prototype.move = function (from, to) {
        this.widget.cols.splice(to, 0, this.widget.cols.splice(from, 1)[0]);
        return this;
    };
    CreatorWidgetTableComponent.prototype.removeCol = function (index) {
        this.widget.cols.splice(index, 1);
    };
    CreatorWidgetTableComponent.prototype.moveLeft = function (index) {
        this.move(index, index - 1);
    };
    CreatorWidgetTableComponent.prototype.moveRight = function (index) {
        this.move(index, index + 1);
    };
    return CreatorWidgetTableComponent;
}());
CreatorWidgetTableComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'creator-widget-table',
                template: "<ul class=\"table\">\n  <li *ngFor=\"let col of widget.cols; let i = index\">\n    <div text-center *ngIf=\"creatorProv.editMode\" text-nowrap>\n      <button small clear ion-button color=\"secondary\" (click)=\"moveLeft(i)\" title=\"Move col left\">\u21E6</button>\n      <button small clear ion-button color=\"secondary\" (click)=\"removeCol(i)\" title=\"Remove col\">\u2297</button>\n      <button small clear ion-button color=\"secondary\" (click)=\"moveRight(i)\" title=\"Move col right\">\u21E8</button>\n    </div>\n    <creator-widgets [(widgets)]=\"widget.cols[i].widgets\"></creator-widgets>\n  </li>\n</ul>"
            },] },
];
CreatorWidgetTableComponent.ctorParameters = function () { return [
    { type: CreatorProvider, },
]; };
var CreatorWidgetTableSettingsComponent =               (function () {
    function CreatorWidgetTableSettingsComponent(navParams) {
        this.navParams = navParams;
        this.data = this.navParams.get('widget');
    }
    CreatorWidgetTableSettingsComponent.prototype.addCol = function () {
        this.data.cols.push({ widgets: [] });
    };
    return CreatorWidgetTableSettingsComponent;
}());
CreatorWidgetTableSettingsComponent.decorators = [
    { type: core.Component, args: [{
                template: "<ion-list no-margin>\n  <button ion-item (click)=\"addCol()\">Spalte hinzuf\u00FCgen</button>\n</ion-list>\n"
            },] },
];
CreatorWidgetTableSettingsComponent.ctorParameters = function () { return [
    { type: ionicAngular.NavParams, },
]; };
var CreatorWidgetsProvider =               (function () {
    function CreatorWidgetsProvider(configCustomWidgets) {
        this.widgets = {
            demo: {
                component: CreatorWidgetDemoComponent,
                model: ModelCreatorWidgetDemo
            },
            headline: {
                component: CreatorWidgetHeadlineComponent,
                model: ModelCreatorWidgetHeadline
            },
            paragraph: {
                component: CreatorWidgetParagraphComponent,
                model: ModelCreatorWidgetParagraph
            },
            image: {
                component: CreatorWidgetImageComponent,
                model: ModelCreatorWidgetImage
            },
            table: {
                component: CreatorWidgetTableComponent,
                model: ModelCreatorWidgetTable
            }
        };
        configCustomWidgets = configCustomWidgets || {};
        Object.assign(this.widgets, configCustomWidgets);
        console.log(this.widgets);
    }
    CreatorWidgetsProvider.prototype.modelize = function (widgets) {
        var _this = this;
        return widgets.filter(function (widget) { return !!_this.widgets[widget.type]; }).map(function (widget) {
            return new _this.widgets[widget.type].model(widget);
        });
    };
    return CreatorWidgetsProvider;
}());
CreatorWidgetsProvider.decorators = [
    { type: core.Injectable },
];
CreatorWidgetsProvider.ctorParameters = function () { return [
    null,
]; };
var CreatorToolsWidgetAddComponent =               (function () {
    function CreatorToolsWidgetAddComponent(_navParams, viewCtrl, creatorWidgets) {
        this._navParams = _navParams;
        this.viewCtrl = viewCtrl;
        this.creatorWidgets = creatorWidgets;
    }
    CreatorToolsWidgetAddComponent.prototype.add = function (type) {
        var widget = this._navParams.get('widgets');
        var widgetModels = this.creatorWidgets.modelize([{
                type: type
            }]);
        widget.push(widgetModels[0]);
        this.viewCtrl.dismiss(widget);
    };
    return CreatorToolsWidgetAddComponent;
}());
CreatorToolsWidgetAddComponent.decorators = [
    { type: core.Component, args: [{
                template: "<ion-list>\n  <button ion-item (click)=\"add(widget.key)\" *ngFor=\"let widget of creatorWidgets.widgets | keys\">\n    {{widget.key}}\n  </button>\n</ion-list>"
            },] },
];
CreatorToolsWidgetAddComponent.ctorParameters = function () { return [
    { type: ionicAngular.NavParams, },
    { type: ionicAngular.ViewController, },
    { type: CreatorWidgetsProvider, },
]; };
var CreatorToolsLoginComponent =               (function () {
    function CreatorToolsLoginComponent(angularFireAuth, _helper, creatorProv) {
        this.angularFireAuth = angularFireAuth;
        this._helper = _helper;
        this.creatorProv = creatorProv;
        this.user = 'i.peguschin@gmail.com';
        this.pwd = 'cskyline';
    }
    CreatorToolsLoginComponent.prototype.login = function () {
        var _this = this;
        this.angularFireAuth.auth.signInWithEmailAndPassword(this.user, this.pwd).then(function () {
            _this._helper.toast('Logged');
        }).catch(function (e) {
            _this._helper.toast('Logging failed');
        });
    };
    return CreatorToolsLoginComponent;
}());
CreatorToolsLoginComponent.decorators = [
    { type: core.Component, args: [{
                template: "<form #loginForm=\"ngForm\" (submit)=\"login()\" margin-bottom *ngIf=\"!creatorProv.user?.uid; else logged\">\n  <ion-list no-margin>\n    <ion-item>\n      <ion-label stacked translate=\"USER.EMAIL.TITLE\" class=\"required\"></ion-label>\n      <ion-input type=\"email\" email required name=\"user\" [(ngModel)]=\"user\"></ion-input>\n    </ion-item>\n    <!--<input-valid [input]=\"userNameModel\"></input-valid>-->\n    <ion-item>\n      <ion-label stacked translate=\"USER.PWD.TITLE\" class=\"required\"></ion-label>\n      <ion-input type=\"password\" required name=\"pwd\" [(ngModel)]=\"pwd\"></ion-input>\n    </ion-item>\n    <!--<input-valid [input]=\"userPwdModel\"></input-valid>-->\n  </ion-list>\n  <div padding-horizontal>\n    <button ion-button block [disabled]=\"!loginForm.valid\">{{ 'GLOBAL.LOGIN' | translate }}</button>\n  </div>\n</form>\n<ng-template #logged>\n  <div text-center margin>\n    Already logged\n    <strong block>{{creatorProv.user?.uid}}</strong>\n  </div>\n</ng-template>\n"
            },] },
];
CreatorToolsLoginComponent.ctorParameters = function () { return [
    { type: auth.AngularFireAuth, },
    { type: HelperProvider, },
    { type: CreatorProvider, },
]; };
var CreatorToolsComponent =               (function () {
    function CreatorToolsComponent(creator, _popoverCtrl) {
        this.creator = creator;
        this._popoverCtrl = _popoverCtrl;
    }
    CreatorToolsComponent.prototype.presentPopover = function (e) {
        e.preventDefault();
        this._popoverCtrl.create(CreatorToolsLoginComponent).present();
    };
    return CreatorToolsComponent;
}());
CreatorToolsComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'creator-tools',
                template: "<ion-fab left bottom *ngIf=\"creator.show\">\n  <button ion-fab (click)=\"presentPopover($event)\"></button>\n</ion-fab>"
            },] },
];
CreatorToolsComponent.ctorParameters = function () { return [
    { type: CreatorProvider, },
    { type: ionicAngular.PopoverController, },
]; };
var CreatorWidgetComponent =               (function () {
    function CreatorWidgetComponent(helperProv, creatorProv, creatorWidgetsProv, componentFactoryResolver, _popoverCtrl) {
        this.helperProv = helperProv;
        this.creatorProv = creatorProv;
        this.creatorWidgetsProv = creatorWidgetsProv;
        this.componentFactoryResolver = componentFactoryResolver;
        this._popoverCtrl = _popoverCtrl;
        this.dataChange = new core.EventEmitter();
        this.remove = new core.EventEmitter();
        this.moveDown = new core.EventEmitter();
        this.moveUp = new core.EventEmitter();
    }
    CreatorWidgetComponent.prototype._loadWidget = function (widget) {
        if (!widget) {
            return false;
        }
        try {
            var componentFactory = this.componentFactoryResolver.resolveComponentFactory(widget.component);
            var componentRef = this.container.createComponent(componentFactory);
            this.componentRefInstance = componentRef.instance;
            this.componentRefInstance['widget'] = this.data;
            componentRef.changeDetectorRef.detectChanges();
        }
        catch (e) {
            console.log(e);
        }
    };
    CreatorWidgetComponent.prototype.ngOnInit = function () {
        this.dataBackup = this.helperProv.copy(this.data);
        this._loadWidget(this.creatorWidgetsProv.widgets[this.data.type]);
    };
    CreatorWidgetComponent.prototype.revertAction = function () {
        this.data = this.helperProv.copy(this.dataBackup);
        this.dataChange.emit(this.data);
    };
    CreatorWidgetComponent.prototype.openSettings = function (event) {
        var popover = this._popoverCtrl.create(this.componentRefInstance.settingsComponent, {
            widget: this.data
        });
        popover.present({
            ev: event
        });
    };
    return CreatorWidgetComponent;
}());
CreatorWidgetComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'creator-widget',
                template: "<ion-toolbar *ngIf=\"creatorProv.editMode\">\n  <!-- <ion-buttons left>\n     <button  ion-button ngxDragHandle title=\"Sort\">\n     \u2725\n     </button>\n   </ion-buttons>-->\n  <ion-title text-uppercase ngxDragHandle>{{data.type}}</ion-title>\n  <ion-buttons end>\n    <button ion-button *ngIf=\"componentRefInstance?.settingsComponent\" (click)=\"openSettings($event)\" title=\"Settings\">\n      \u270E\n    </button>\n    <button ion-button\n            [disabled]=\"helperProv.compare(dataBackup, data)\"\n            (click)=\"revertAction()\"\n            title=\"Revert changes\">\u21BA\n    </button>\n    <button ion-button (click)=\"moveUp.emit()\" title=\"Move up\">\n      \u21E7\n    </button>\n    <button ion-button (click)=\"moveDown.emit()\" title=\"Move down\">\n      \u21E9\n    </button>\n    <button ion-button (click)=\"remove.emit()\" title=\"Remove\">\n      \u2718\n    </button>\n  </ion-buttons>\n</ion-toolbar>\n<ng-container #widgetPlace></ng-container>"
            },] },
];
CreatorWidgetComponent.ctorParameters = function () { return [
    { type: HelperProvider, },
    { type: CreatorProvider, },
    { type: CreatorWidgetsProvider, },
    { type: core.ComponentFactoryResolver, },
    { type: ionicAngular.PopoverController, },
]; };
CreatorWidgetComponent.propDecorators = {
    "container": [{ type: core.ViewChild, args: ['widgetPlace', { read: core.ViewContainerRef },] },],
    "data": [{ type: core.Input },],
    "dataChange": [{ type: core.Output },],
    "remove": [{ type: core.Output },],
    "moveDown": [{ type: core.Output },],
    "moveUp": [{ type: core.Output },],
};
var CreatorWidgetsComponent =               (function () {
    function CreatorWidgetsComponent(_helperProv, _popoverCtrl, creatorProv) {
        this._helperProv = _helperProv;
        this._popoverCtrl = _popoverCtrl;
        this.creatorProv = creatorProv;
        this.widgetsChange = new core.EventEmitter();
        this.actionLog = new Subject.Subject();
    }
    CreatorWidgetsComponent.prototype.log = function (event) {
        var data = {
            action: event.type,
            widget: event.value
        };
        this.actionLog.next(data);
    };
    CreatorWidgetsComponent.prototype._emit = function () {
        this.widgetsChange.emit(this.widgets);
    };
    CreatorWidgetsComponent.prototype.moveWidget = function (fromIndex, toIndex) {
        var element = this.widgets[fromIndex];
        this.widgets.splice(fromIndex, 1);
        this.widgets.splice(toIndex, 0, element);
        this._emit();
    };
    CreatorWidgetsComponent.prototype.removeWidget = function (index) {
        this.widgets.splice(index, 1);
        this._emit();
    };
    CreatorWidgetsComponent.prototype.addWidget = function (event) {
        var _this = this;
        var popover = this._popoverCtrl.create(CreatorToolsWidgetAddComponent, {
            widgets: this.widgets
        });
        popover.present({
            ev: event,
            updateUrl: false
        });
        popover.onDidDismiss(function (res) { return _this._emit(); });
    };
    CreatorWidgetsComponent.prototype.exportImport = function () {
        var data = JSON.parse(window.prompt("Enter export from clipboard: Ctrl+V, Enter", JSON.stringify(this.widgets)));
        if (data) {
            this.widgets = data;
            this._emit();
        }
    };
    return CreatorWidgetsComponent;
}());
CreatorWidgetsComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'creator-widgets',
                template: "<div class=\"ngx-dnd-container\" ngxDroppable=\"widgets\" [ngClass]=\"{'gu-empty': !widgets.length}\" [model]=\"widgets\"\n     (drag)=\"log($event)\"\n     (drop)=\"log($event)\"\n     (over)=\"log($event)\"\n     (out)=\"log($event)\"\n     (remove)=\"log($event)\">\n  <creator-widget ngxDraggable class=\"ngx-dnd-item has-handle\"\n                  *ngFor=\"let widget of widgets; let i = index\"\n                  [model]=\"widgets[i]\"\n                  [(data)]=\"widgets[i]\"\n                  (remove)=\"removeWidget(i)\"\n                  (moveDown)=\"moveWidget(i, i+1)\"\n                  (moveUp)=\"moveWidget(i, i-1)\"></creator-widget>\n</div>\n<ion-toolbar color=\"dark\" *ngIf=\"creatorProv.editMode\">\n  <ion-buttons left>\n    <button ion-button (click)=\"exportImport($event)\" title=\"Export/Import\">\n      &#123; &#125;\n    </button>\n  </ion-buttons>\n  <!--<ion-title text-uppercase>{{}}</ion-title>-->\n  <ion-buttons end>\n    <button ion-button (click)=\"addWidget($event)\">\n      \u2295\n    </button>\n  </ion-buttons>\n</ion-toolbar>\n"
            },] },
];
CreatorWidgetsComponent.ctorParameters = function () { return [
    { type: HelperProvider, },
    { type: ionicAngular.PopoverController, },
    { type: CreatorProvider, },
]; };
CreatorWidgetsComponent.propDecorators = {
    "widgets": [{ type: core.Input },],
    "widgetsChange": [{ type: core.Output },],
};
var LocalstorageProvider =               (function () {
    function LocalstorageProvider() {
    }
    LocalstorageProvider.prototype.get = function (key, toJSON) {
        if (toJSON === void 0) { toJSON = true; }
        var c = localStorage.getItem(key);
        return (toJSON && c) ? JSON.parse(c) : localStorage.getItem(key);
    };
    LocalstorageProvider.prototype.set = function (key, data, toString) {
        if (toString === void 0) { toString = true; }
        return (toString) ? localStorage.setItem(key, JSON.stringify(data)) : localStorage.setItem(key, data);
    };
    LocalstorageProvider.prototype.remove = function (key) {
        return localStorage.removeItem(key);
    };
    return LocalstorageProvider;
}());
LocalstorageProvider.decorators = [
    { type: core.Injectable },
];
var CreatorComponent =               (function () {
    function CreatorComponent(creator, helperProv, _localStorageProv) {
        this.creator = creator;
        this.helperProv = helperProv;
        this._localStorageProv = _localStorageProv;
        this.editMode = false;
        this._history = [];
        this.newData = new ModelCreatorWidgetPlace();
    }
    CreatorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._reset();
        this.historyObserver$ = this.creator.getWidgetPlaceHistory(this.name).map(function (res) {
            _this._history = res;
            return res;
        });
        this.widgetsSubscription$ = this.creator.getActiveWidgetPlace(this.name).subscribe(function (res) {
            _this._localStorageProv.remove(_this.name);
            if (res) {
                _this._localStorageProv.set(_this.name, res);
                _this.currentActiveContent = res;
                _this.setData(res);
            }
            else {
                _this._reset();
            }
        });
    };
    CreatorComponent.prototype.ngOnDestroy = function () {
        if (this.widgetsSubscription$)
            this.widgetsSubscription$.unsubscribe();
    };
    CreatorComponent.prototype.setData = function (data) {
        this.originalData = data;
        this.tmpOriginalData = this.helperProv.copy(this.originalData);
    };
    CreatorComponent.prototype.selectHistory = function (id) {
        var historyData = this._history.find(function (entry) { return entry.id === id; });
        this.setData(historyData);
    };
    CreatorComponent.prototype._reset = function () {
        var storage = this._localStorageProv.get(this.name);
        var tmp = storage ? new ModelCreatorWidgetPlace(storage) : new ModelCreatorWidgetPlace();
        this.setData(tmp);
    };
    CreatorComponent.prototype.update = function () {
        var _this = this;
        this.tmpOriginalData.data.active = new Date(this.tmpOriginalData.data.active);
        this.tmpOriginalData.data.widgets = this.helperProv.copy(this.tmpOriginalData.data.widgets);
        if (this.tmpOriginalData.id) {
            this.creator.update(this.name, this.tmpOriginalData.id, this.tmpOriginalData.data).then(function (res) {
                _this.helperProv.toast('History updated');
                _this.selectHistory(_this.tmpOriginalData.id);
            });
        }
        else {
            this.creator.add(this.name, this.tmpOriginalData.data).then(function (res) {
                _this.helperProv.toast('History added');
                _this.selectHistory(res.id);
            });
        }
    };
    CreatorComponent.prototype.revertAll = function () {
        return this.tmpOriginalData = this.helperProv.copy(this.originalData);
    };
    CreatorComponent.prototype.compareFn = function (e1, e2) {
        return e1 && e2 ? e1.id === e2.id : e1 === e2;
    };
    CreatorComponent.prototype.deleteHistory = function () {
        var _this = this;
        var confirm = window.confirm('Are you sure you want to delete it?');
        if (confirm) {
            this.creator.delete(this.name, this.tmpOriginalData.id).then(function (res) {
                _this._reset();
            });
        }
    };
    return CreatorComponent;
}());
CreatorComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'creator',
                template: "<div [ngClass]=\"{'editMode' : creator.editMode}\" *ngIf=\"tmpOriginalData\">\n  <ion-toolbar *ngIf=\"creator.editMode\" color=\"dark\">\n    <ion-buttons left>\n      <button ion-button small (click)=\"editMode = !editMode\">\u270E</button>\n    </ion-buttons>\n    <ion-grid no-padding>\n      <ion-row>\n        <ion-col>\n          <ion-item color=\"dark\" no-lines>\n            <ion-label>Template</ion-label>\n            <ion-select [ngModel]=\"tmpOriginalData\" [compareWith]=\"compareFn\" #history (ionChange)=\"setData(history.value)\">\n              <ion-option [value]=\"newData\">New template</ion-option>\n              <ion-option *ngFor=\"let item of historyObserver$ | async\" [value]=\"item\">\n                {{item.data.name || 'Template X'}}\n                <span *ngIf=\"helperProv.compare(item, currentActiveContent)\">\u25C9</span>\n              </ion-option>\n            </ion-select>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n    <ion-buttons right>\n      <button clear ion-button [disabled]=\"helperProv.compare(originalData, tmpOriginalData)\" (click)=\"revertAll()\">\n        \u21BA\n      </button>\n      <button clear ion-button [disabled]=\"helperProv.compare(originalData, tmpOriginalData)\" (click)=\"update()\">\n        \u2714\n      </button>\n    </ion-buttons>\n    <div *ngIf=\"editMode\">\n      <ion-item color=\"dark\">\n        <ion-label stacked class=\"required\">Name</ion-label>\n        <ion-input name=\"historyEntryName\" type=\"text\" [(ngModel)]=\"tmpOriginalData.data.name\" required></ion-input>\n      </ion-item>\n      <ion-item color=\"dark\">\n        <ion-label stacked>Active from</ion-label>\n        <ion-datetime [(ngModel)]=\"tmpOriginalData.data.active\"\n                      displayFormat=\"DD.MM.YYYY HH:mm UTC\"\n                      pickerFormat=\"DD.MM.YYYYTHH:mm\"\n                      doneText=\"{{ 'BUTTON.SAVE' | translate }}\"\n                      cancelText=\"{{ 'BUTTON.ABORT' | translate }}\"></ion-datetime>\n      </ion-item>\n      <ion-toolbar>\n        <ion-buttons>\n          <button ion-button *ngIf=\"tmpOriginalData.id\" (click)=\"deleteHistory()\" color=\"danger\" title=\"Delete\">Delete</button>\n        </ion-buttons>\n      </ion-toolbar>\n    </div>\n  </ion-toolbar>\n  <creator-widgets [(widgets)]=\"tmpOriginalData.data.widgets\"></creator-widgets>\n</div>"
            },] },
];
CreatorComponent.ctorParameters = function () { return [
    { type: CreatorProvider, },
    { type: HelperProvider, },
    { type: LocalstorageProvider, },
]; };
CreatorComponent.propDecorators = {
    "name": [{ type: core.Input },],
};
var CreatorTextDirective =               (function () {
    function CreatorTextDirective(creatorProv, _el) {
        var _this = this;
        this.creatorProv = creatorProv;
        this._el = _el;
        this._changeEditMode = function (editMode) {
            if (!_this.disabled) {
                if (editMode) {
                    _this._el.nativeElement.setAttribute('contenteditable', true);
                }
                else {
                    _this._el.nativeElement.removeAttribute('contenteditable');
                }
            }
        };
        this.propagateChange = function (_) {
        };
        this.OnChange = new core.EventEmitter();
        this._disabled = false;
    }
    CreatorTextDirective.prototype.ngOnInit = function () {
        this._changeEditMode(this.creatorProv.editMode);
        this._onSwitchSubscription = this.creatorProv.OnSwitchEditMode.subscribe(this._changeEditMode);
    };
    CreatorTextDirective.prototype.ngOnDestroy = function () {
        this._onSwitchSubscription.unsubscribe();
    };
    CreatorTextDirective.prototype.registerOnChange = function (fn) {
        this.propagateChange = fn;
    };
    CreatorTextDirective.prototype.registerOnTouched = function () {
    };
    CreatorTextDirective.prototype.writeValue = function (value) {
        if (value !== undefined) {
            this.inputValue = value;
            this._el.nativeElement.innerText = this.inputValue;
        }
    };
    Object.defineProperty(CreatorTextDirective.prototype, "disabled", {
        get: function () {
            return (this._disabled === '') ? true : !!this._disabled;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreatorTextDirective.prototype, "inputValue", {
        get: function () {
            return this._inputValue;
        },
        set: function (val) {
            this._inputValue = (isNaN(val)) ? val : Number.parseInt(val);
        },
        enumerable: true,
        configurable: true
    });
    CreatorTextDirective.prototype.onKeyup = function () {
        this.inputValue = this._el.nativeElement.innerText;
        this.propagateChange(this._inputValue);
    };
    return CreatorTextDirective;
}());
CreatorTextDirective.decorators = [
    { type: core.Directive, args: [{
                selector: '[creator-text]',
                host: {
                    '(input)': 'onKeyup()'
                },
                providers: [
                    {
                        provide: forms.NG_VALUE_ACCESSOR,
                        multi: true,
                        useExisting: core.forwardRef(function () { return CreatorTextDirective; }),
                    }
                ]
            },] },
];
CreatorTextDirective.ctorParameters = function () { return [
    { type: CreatorProvider, },
    { type: core.ElementRef, },
]; };
CreatorTextDirective.propDecorators = {
    "_inputValue": [{ type: core.Input },],
    "OnChange": [{ type: core.Output, args: ['change',] },],
    "_disabled": [{ type: core.Input, args: ['disabled',] },],
};
var KeysPipe =               (function () {
    function KeysPipe() {
    }
    KeysPipe.prototype.transform = function (value, args) {
        return Object.keys(value).map(function (key) {
            return { key: key, value: value[key] };
        });
    };
    return KeysPipe;
}());
KeysPipe.decorators = [
    { type: core.Pipe, args: [{
                name: 'keys',
            },] },
];
var SafehtmlPipe =               (function () {
    function SafehtmlPipe(_sanitizer) {
        this._sanitizer = _sanitizer;
    }
    SafehtmlPipe.prototype.transform = function (value) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        return this._sanitizer.bypassSecurityTrustHtml(value);
    };
    return SafehtmlPipe;
}());
SafehtmlPipe.decorators = [
    { type: core.Pipe, args: [{
                name: 'safehtml',
            },] },
];
SafehtmlPipe.ctorParameters = function () { return [
    { type: platformBrowser.DomSanitizer, },
]; };
var TrustedurlPipe =               (function () {
    function TrustedurlPipe(_sanitizer) {
        this._sanitizer = _sanitizer;
    }
    TrustedurlPipe.prototype.transform = function (value) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        return this._sanitizer.bypassSecurityTrustResourceUrl(value);
    };
    return TrustedurlPipe;
}());
TrustedurlPipe.decorators = [
    { type: core.Pipe, args: [{
                name: 'trustedurl',
            },] },
];
TrustedurlPipe.ctorParameters = function () { return [
    { type: platformBrowser.DomSanitizer, },
]; };
var PipesModule =               (function () {
    function PipesModule() {
    }
    return PipesModule;
}());
PipesModule.decorators = [
    { type: core.NgModule, args: [{
                declarations: [
                    KeysPipe,
                    SafehtmlPipe,
                    TrustedurlPipe,
                ],
                imports: [],
                exports: [
                    KeysPipe,
                    SafehtmlPipe,
                    TrustedurlPipe,
                ]
            },] },
];
var CreatorModule =               (function () {
    function CreatorModule() {
    }
    CreatorModule.forRoot = function (config) {
        return {
            ngModule: CreatorModule,
            providers: [
                CreatorProvider,
                {
                    provide: CreatorWidgetsProvider,
                    useValue: new CreatorWidgetsProvider(config.customWidgets)
                }
            ]
        };
    };
    return CreatorModule;
}());
CreatorModule.decorators = [
    { type: core.NgModule, args: [{
                declarations: [
                    CkeditorComponent,
                    CreatorTextDirective,
                    CreatorComponent,
                    CreatorToolsComponent,
                    CreatorToolsLoginComponent,
                    CreatorToolsWidgetAddComponent,
                    CreatorWidgetComponent,
                    CreatorWidgetDemoComponent,
                    CreatorWidgetDemoSettingsComponent,
                    CreatorWidgetImageComponent,
                    CreatorWidgetImageSettingsComponent,
                    CreatorWidgetTableComponent,
                    CreatorWidgetTableSettingsComponent,
                    CreatorWidgetParagraphComponent,
                    CreatorWidgetHeadlineComponent,
                    CreatorWidgetHeadlineSettingsComponent,
                    CreatorWidgetsComponent,
                ],
                entryComponents: [
                    CreatorToolsLoginComponent,
                    CreatorToolsWidgetAddComponent,
                    CreatorWidgetDemoComponent,
                    CreatorWidgetDemoSettingsComponent,
                    CreatorWidgetImageComponent,
                    CreatorWidgetImageSettingsComponent,
                    CreatorWidgetTableComponent,
                    CreatorWidgetTableSettingsComponent,
                    CreatorWidgetParagraphComponent,
                    CreatorWidgetHeadlineComponent,
                    CreatorWidgetHeadlineSettingsComponent,
                ],
                providers: [
                    CreatorProvider,
                    HelperProvider,
                    LocalstorageProvider
                ],
                imports: [
                    PipesModule,
                    core$1.TranslateModule.forRoot(),
                    http.HttpClientModule,
                    angularfire2.AngularFireModule,
                    firestore.AngularFirestoreModule,
                    auth.AngularFireAuthModule,
                    ionicAngular.IonicModule,
                    common.CommonModule,
                    ngxDnd.NgxDnDModule
                ],
                exports: [
                    CreatorComponent,
                    CreatorToolsComponent
                ]
            },] },
];

exports.CreatorModule = CreatorModule;
exports.CreatorProvider = CreatorProvider;
exports.ModelCreatorWidget = ModelCreatorWidget;
exports.ModelCreatorWidgetHeadline = ModelCreatorWidgetHeadline;
exports.ModelCreatorWidgetParagraph = ModelCreatorWidgetParagraph;
exports.ModelCreatorWidgetImage = ModelCreatorWidgetImage;
exports.ModelCreatorWidgetDemo = ModelCreatorWidgetDemo;
exports.ModelCreatorWidgetTable = ModelCreatorWidgetTable;
exports.ModelCreatorWidgetPlace = ModelCreatorWidgetPlace;
exports.ɵa = CkeditorComponent;
exports.ɵh = CreatorToolsComponent;
exports.ɵg = CreatorToolsLoginComponent;
exports.ɵf = CreatorToolsWidgetAddComponent;
exports.ɵk = CreatorWidgetDemoComponent;
exports.ɵl = CreatorWidgetDemoSettingsComponent;
exports.ɵr = CreatorWidgetHeadlineComponent;
exports.ɵs = CreatorWidgetHeadlineSettingsComponent;
exports.ɵm = CreatorWidgetImageComponent;
exports.ɵn = CreatorWidgetImageSettingsComponent;
exports.ɵq = CreatorWidgetParagraphComponent;
exports.ɵo = CreatorWidgetTableComponent;
exports.ɵp = CreatorWidgetTableSettingsComponent;
exports.ɵj = CreatorWidgetComponent;
exports.ɵt = CreatorWidgetsComponent;
exports.ɵd = CreatorComponent;
exports.ɵc = CreatorTextDirective;
exports.ɵv = KeysPipe;
exports.ɵu = PipesModule;
exports.ɵw = SafehtmlPipe;
exports.ɵx = TrustedurlPipe;
exports.ɵi = CreatorWidgetsProvider;
exports.ɵb = HelperProvider;
exports.ɵe = LocalstorageProvider;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=ionic.creator.umd.js.map
